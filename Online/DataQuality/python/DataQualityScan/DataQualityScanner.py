import os, sys, time, traceback
from DbAccess import MonDB, RunDB
from DIM import IntegerService, RunFileClient, Controller, FSM
from DataQualityScan import *

NUMBER_OF_DAYS=100
NUMBER_OF_DAYS=30

# ------------------------------------------------------------------------------
class Setup:
  """
        Setup module for the data quality monitoring
        Member functions perform the creation 
        of the database and the working directories

        \author  M.Frank
        \version 1.0
  """
  # ----------------------------------------------------------------------------
  def __init__(self, config):
    """ Initializing constructor

        \author  M.Frank
        \version 1.0
    """
    from OnlineKernel.DbCore import DbCore
    self.config = config
    self.DbCore = DbCore

  # ----------------------------------------------------------------------------
  def createTables(self):
    """ Create all tables to monitor the data quality monitoring facility

        \author  M.Frank
        \version 1.0
    """
    self._createTable(self.config.mondb_archive_table.upper())
    return self._createTable(self.config.mondb_table.upper())

  # ----------------------------------------------------------------------------
  def _createTable(self, table):
    """ Create all tables to monitor the data quality monitoring facility

        \author  M.Frank
        \version 1.0
    """
    db     = self.DbCore(self.config.mondb);
    cursor = db.connection().cursor()
    text = db.text_t(255)
    integer = db.int_t(12)
    stmt = ""
    try:
      stmt = """CREATE TABLE %s (runid INTEGER PRIMARY KEY, 
                                 state %s,
                                 todo INTEGER, 
                                 done INTEGER,
                                 checked INTEGER,
                                 archived INTEGER, 
                                 partitionid INTEGER,
                                 partitionname %s,
                                 activity %s,
                                 runtype %s,
                                 params %s,
                                 starttime INTEGER) """%(table,text,text,text,text,text,)
      cursor.execute(stmt)
      cursor.close()
      db.connection().commit()
    except Exception, X:
      log(INFO,'Failed to create table: :"%s" with statement:%s'%(table,stmt,))
      log(INFO,'Failure:  '+str(X))
    return self

  # ----------------------------------------------------------------------------
  def createDirectories(self):
    """ Create all working directories used by the data quality monitoring

        \author  M.Frank
        \version 1.0
    """
    make_directory(self.config.optsDirectory)
    make_directory(self.config.linkDirectory)
    return self

  # ----------------------------------------------------------------------------
  def printSummary(self):
    """ Print summary information about the facility environment

        \author  M.Frank
        \version 1.0
    """
    import sys
    table = self.config.mondb_table
    db = self.DbCore(self.config.mondb).connection()
    cursor = db.cursor()
    try:
      cursor.execute("""SELECT COUNT(state) FROM %s"""%(table,))
    except Exception, X:
      log(INFO,'Failed to execute SQL:'+str(X))
    count = cursor.fetchone()
    cursor.close()
    log(INFO,'+%s+'%(110*'-',))
    log(INFO,'|  Working directory:   %-85s  |'%(os.getcwd(),))
    log(INFO,'|  Monitoring database: %-85s  |'%(self.config.mondb,))
    log(INFO,'|  dto. Number of entries: %-82s  |'%(str(count),))
    log(INFO,'+%s+'%(110*'-',))
    #sys.exit(0)

# ------------------------------------------------------------------------------
class DataQualityScanner:
  """
  Data file scanner to steer the data quality processing after HLT2.
  - This class scan the run database for recent runs of the LHCb partition
    --> self.scanDatabase(<partition>)

  \author  M.Frank
  \version 1.0
  """

  # ----------------------------------------------------------------------------
  def __init__(self, config, client):
    """ Initializing constructor of the file scanner

    \author  M.Frank
    \version 1.0
    """
    self.config = config
    self.mondb  = MonDB(config=self.config,
                        db=self.config.mondb,
                        table=self.config.mondb_table)
    self.rundb  = RunDB(config=self.config,
                        db=self.config.rundb)
    self.farmClient          = client
    self.runs_rejected       = {}
    self.runs_noprint        = {}
    self.run_modules         = {}
    
  # ----------------------------------------------------------------------------
  def chooseFileStreamName(self, run):
    """
    Decide which file stream to use depending on the run DataType.

    \author  M.Adinolfi
    \version 1.0
    """
    log(DEBUG,'++ Run %d: looking for file stream name'%(run,))
    mod = self.runModule(run)
    if not mod:
      self.run_warn(run,WARNING,'Run %d is rejected: cannot choose stream [Bad HLT2Params]'%(run,))
      return False

    if not hasattr(mod,'DataType'):
      self.run_warn(run,WARNING,'Run %d is rejected: cannot choose stream (no DataTpe attribute) [Bad HLT2Params]'%(run,))
      return False

    dataType       = mod.DataType
    fileStreamName = self.config.file_stream_name
    if dataType == 'PROTONION16':
      fileStreamName = 'TURBO'
    elif dataType == 'IONPROTON16':
      fileStreamName = 'TURBO'
    elif dataType == 'PROTONHELIUM16':
      fileStreamName = 'SMOGPHY'
    elif dataType == 'XENON17':
      fileStreamName = 'FULL'
    elif dataType == 'LEAD18':
      fileStreamName = 'FULL'

    log(DEBUG,'++ Run %d: file stream name = %s'%(run,fileStreamName,))

    return fileStreamName
  # ----------------------------------------------------------------------------
  def runModule(self, run):
    """
    Create a python module containing the run parameters for the 
    data quality file processing

    \author  M.Frank
    \version 1.0
    """
    import sys, imp
    mod_name = 'Run_%d'%(run,)
    if not self.run_modules.has_key(mod_name):
      if not sys.modules.has_key(mod_name):
        source_path = '%s/%d/%d/%s'%(self.config.condDirectory,int(run)/1000,int(run),'HLT2Params.py',)
        mod = imp.load_source(mod_name,source_path)
        self.run_modules[mod_name] = mod
    return self.run_modules[mod_name]
    

  # ----------------------------------------------------------------------------
  def selectRecentRuns(self, partition='LHCb', num_of_days=NUMBER_OF_DAYS):
    """
    Select all runs taken by partition <partition> during the last num_of_days days.

    \author  M.Frank
    \version 1.0
    """
    stmt = """SELECT r.runid AS runid 
              FROM   rundbruns r
              WHERE  r.partitionname=:partitionname 
                AND (SYSDATE-r.starttime)<:starttime AND r.state>1
              ORDER BY r.runid ASC"""
    param= { 'partitionname': partition, 'starttime': num_of_days }
    cursor = self.rundb.execute(stmt,params=param)
    info = cursor.fetchall()
    cursor.close()
    if info is not None:
      info = [i[0] for i in info]
      info.sort()
      return info
    return None

  # ----------------------------------------------------------------------------
  def addTodo(self, run_no, directory):
    """
    Insert a new record into the data quality monitoring database 
    for work....

        \author  M.Frank
        \version 1.0
    """
    info = self.rundb.runInfo(run_no)
    if info is not None:
      part_id, part_name, runtype, activity, starttime = info
      param = {'runid':              run_no, 
               'state':              self.config.st_todo,
               'todo':               int(time.time()), 
               'done':               0, 
               'checked':            0,
               'archived':           0,
               'partitionid':        part_id,
               'partitionname':      part_name, 
               'activity':           activity, 
               'runtype':            runtype, 
               'params':             directory,
               'starttime':          starttime}
      stmt = ""
      # Now insert the record into the database
      try:
        stmt = """INSERT INTO %s 
                  VALUES (:runid, :state, :todo, :done, :checked, :archived, 
                          :partitionid, :partitionname, :activity, 
                          :runtype, :params, :starttime)"""%(self.mondb.table,)
        ##print "INSERT INTO ",self.mondb.table," VALUES ",str(param)
        cursor = self.mondb.execute(stmt, params=param)
        self.mondb.commit()
      except Exception,X:
        log(WARNING,'Skip directory %s [No data files:%s] '%(path,str(X)))
        return None
      return self
    log(WARNING,'Run %d does not exist in the run database [Inconsistent data]'%(run_no,))
    return None

  # ----------------------------------------------------------------------------
  def dump(self):
    """
    \author  M.Frank
    \version 1.0
    """
    cursor = self.mondb.cursor()
    cursor.execute("SELECT * FROM %s WHERE runid<>0"%(self.mondb.table,))
    data = cursor.fetchone()
    while data is not None:
      path = self.config.daqarea_directory+os.sep+data[FIELD_PARTNAME]+\
          os.sep+data[FIELD_RUNTYPE]+os.sep+str(data[FIELD_RUNID])
      files = os.listdir(path)
      print 'DUMP:',str(data), len(files),' Files'      
      data = cursor.fetchone()
    cursor.close()
    return self

  # -----------------------------------------------------------------------------
  def run_warn(self, run, lvl, msg):
    """
    \author  M.Frank
    \version 1.0
    """
    if not self.runs_noprint.has_key(run):
      self.runs_noprint[run] = 1
      log(lvl,msg)

  # -----------------------------------------------------------------------------
  def scanDatabase(self, partition='LHCb', num_of_days=NUMBER_OF_DAYS):
    """
    Scan the run database and match all necessary information to populate
    the monitoring database

    \author  M.Frank
    \version 1.0
    """
    import math
    recent_runs  = self.selectRecentRuns(partition, num_of_days)
    runs_pending = self.farmClient.getData()
    not_enough_events = 0

    for run in recent_runs:
      # If this run was rejected before, save us the work.
      if self.runs_rejected.has_key(run):
        continue

      srun = str(run)
      params = '%s/%d/%d/%s'%(self.config.condDirectory,int(run)/1000,int(run),'HLT2Params.py',)

      # First check if the params file is present:
      hlt2_params = os.access(params,os.R_OK)
      if not hlt2_params:
        #log(WARNING,'++ Run %s has no file %s' %(srun, params))
        continue

      # If the run is already in the monitoring DB, ignore it:
      in_mondb = self.mondb.hasRun(run)
      if in_mondb:
        continue

      ##log(INFO,"Checking run number:%d"%(run,));
      # Now check if a minimal number of files is present overall
      mod = self.runModule(run)
      if not mod:
        # This run will never be processed. There are simply no data
        self.runs_rejected[run] = 1
        self.run_warn(run,WARNING,'Run %d is rejected [Bad HLT2Params]'%(run,))
        continue

      if not hasattr(mod,'RunNFiles'):
        # This run will never be processed. There are simply no data
        self.runs_rejected[run] = 1
        self.run_warn(run,WARNING,'Run %d is rejected (no RunNFiles attribute) [Bad HLT2Params]'%(run,))
        continue

      num_files_total = float(mod.RunNFiles)
      if not num_files_total > 0:
        # This run will never be processed. There are simply no data
        self.runs_rejected[run] = 1
        self.run_warn(run,WARNING,'Run %d is rejected [No-files]'%(run,))
        continue

      # Now check if a sufficient amount of files was processed:
      if not runs_pending.has_key(srun):
        num_files_waiting = 0.0
      elif runs_pending.has_key(srun):
        num_files_waiting = float(int(runs_pending[srun]))
      if num_files_total < num_files_waiting:
        num_files_total = num_files_waiting
      ratio = float(num_files_waiting)/float(num_files_total)
      ratio = math.fabs(1.0 - ratio)
      if ratio < self.config.min_files_processed:
        self.run_warn(run,WARNING,'Run %d is rejected [Not-enough files abs(1-%d/%d)=%.3f < %.3f]'%\
                        (run,num_files_waiting,num_files_total, ratio,
                         self.config.min_files_processed,))
        continue

      fileStreamName = self.chooseFileStreamName(run)
      if not fileStreamName:
        continue
      # This is a new run to be processed:
      #files = self.rundb.runFiles(run, self.config.file_stream_name)
      files = self.rundb.runFiles(run, fileStreamName)

      # Check if the number of output files is OK:
      num_events = 0
      num_files = 0
      usable_files = []
      for f in files:
        file_name = f[0]+os.sep+f[1]
        if os.access(file_name,os.R_OK):
          num_files  = num_files + 1
          num_events  = num_events + f[3]
          usable_files.append(f)

      # Check if the number of events is above threshold
      if not num_events > self.config.min_event_count:
        not_enough_events = not_enough_events+1
        self.run_warn(run,WARNING,'Run %d is rejected [Not-enough events: %d<%d]'%(run,num_events,self.config.min_event_count,))
        continue

      msg = 'Run:%d In MonDb:%s Files waiting:%4d total:%4d daq-area:%3d Events:%10d'%\
          (run,str(in_mondb),num_files_waiting,num_files_total,len(usable_files),num_events,)
      if len(usable_files):
        log(ALWAYS,'+++ Add todo '+msg)
        self.addTodo(run,directory=files[0][0])
      elif not self.runs_rejected.has_key(run):
        self.runs_rejected[run] = 1
        self.run_warn(run,INFO,'+++ REJECTED '+msg)

    #if not_enough_events > 0:
    #  log(WARNING,'Rejected %d runs with not-enough events.'%(not_enough_events,))
    return self

  # -----------------------------------------------------------------------------
  def restart(self):
    """
    Check if there is old work left after a restart. If yes, restart these runs
    Update the monitoring database corrspondingly.

    \author  M.Frank
    \version 1.0
    """
    running_runs = self.mondb.runsByState(state=self.config.st_running)
    for run in running_runs:
      log(INFO,'+++ Reschedule run %d after restart.'%(run,))
      self.mondb.setRunTodo(run)
    log(INFO,'+++ Restart: Got %d runs in state %s.'%(len(running_runs),self.config.st_running,))
    return len(running_runs)>0

  # -----------------------------------------------------------------------------
  def _prepareRun(self, run, with_options=False):
    """
    Prepare a single run for execution.
    Update the monitoring database corrspondingly.

    \author  M.Frank
    \version 1.0
    """
    import errno
    runno = int(str(run))
    if run:
      fileStreamName = self.chooseFileStreamName(run)
      if not fileStreamName:
        log(WARNING,'++ CANNOT prepare work for run %d. [No file stream name defined]'%(runno,))
        return None
      
      #files = self.rundb.runFiles(run=run,stream=self.config.file_stream_name)
      files = self.rundb.runFiles(run=run,stream=fileStreamName)
      # Check if there are any files to be processed
      if not len(files):
        return None

      num_evt = 0
      num_file = 0
      used_files = []
      source_dir_name = ''
      for f in files:
        try:
          source_dir_name = f[0].replace('/daqarea2/','/daqarea/')
          file_name = source_dir_name+os.sep+f[1]
          if os.access(file_name,os.R_OK):
            num_file  = num_file + 1
            num_evt   = num_evt + f[3]
            used_files.append(f)
            continue
        except Exception,X:
          log(ERROR,'++ Exception: Run %d cannot be prepare file: %s []'%(runno,str(X),))

      num_linked = 0
      daqarea_files = None
      try:
        log(ERROR,'++ Prepare work for run %d. [Directory:%s]'%(runno,source_dir_name,))
        daqarea_files = os.listdir(source_dir_name)
      except:
        pass

      if not daqarea_files:
        log(WARNING,'++ CANNOT prepare work for run %d. [No work to do]'%(runno,))
        log(WARNING,'++ Run %s cannot be processed.... setting state to FAILED.'%(str(run),))
        self.mondb.setRunFailed(runno)
        return None
      
      debug_links = False
      if debug_links:
        log(ERROR,'++ 0-- Prepare work for run %d. [%s]'%(runno,str(daqarea_files),))
      for f in daqarea_files:
        try:
          if debug_links:
            log(ERROR,'++ 1-- Prepare file for run %d. [%s]'%(runno,str(f),))
          file_name = source_dir_name+os.sep+f
          if debug_links:
            log(ERROR,'++ 2-- Prepare file for run %d. [%s]'%(runno,str(f),))
          link_name = self.config.linkDirectory+os.sep+'Run_'+f
          if debug_links:
            log(ERROR,'++ 3-- Prepare work for run %d. Access %s -> %s'%(runno,f,file_name,))
          if os.access(file_name,os.R_OK):
            if debug_links:
              log(ERROR,'++ 4-- Prepare work for run %d. Access %s -> %s'%(runno,f,link_name,))
            if os.access(link_name,os.R_OK):
              if debug_links:
                log(ERROR,'++ 5-- Prepare work for run %d. Readlink %s -> %s'%(runno,f,link_name,))
              if os.readlink(link_name) == link_name:
                if debug_links:
                  log(ERROR,'++ 6-- Prepare work for run %d. Link OK %s'%(runno,link_name,))
                continue
              else:
                os.unlink(link_name)
            if debug_links:
              log(ERROR,'++ 7-- Prepare work for run %d. CREATE Link %s -> %s'%(runno,link_name,file_name,))
            os.symlink(file_name, link_name)
            log(WARNING,'++ Run %d prepare input  file:%s (%s)'%(runno,link_name,file_name,))
            num_linked = num_linked + 1
            continue
          err = ''
          try:
            os.stat(link_name)
          except Exception,X:
            err = str(X)
            log(ERROR,'++ Run %d cannot be prepare file: %s (%s) [IGNORE]'%(runno,link_name,err,))
        except Exception,X:
          log(ERROR,'++ Exception: Run %d cannot be prepare file: %s [%s]'%(runno,str(X),str(f),))
          #return None

      files = []
      for u in used_files:
        for f in daqarea_files:
          if f[0] == u:
            files.append(f)
            break
      used_files = files
      # If the file in the DAQAREA is not known, approximate the number of events.
      for f in daqarea_files:
        skip = False
        for u in used_files:
          if f[0] == u:
            skip = True
            break
        if skip: continue
        file_name = source_dir_name+os.sep+f
        num_evt = int(float(os.stat(file_name).st_size)/50./1024)
        log(WARNING,'++ Run %d use merged file:%s with [%8d events]'%(runno,file_name,num_evt,))
        used_files.append([source_dir_name,f,0,num_evt])

      if debug_links:
        log(ERROR,'++ END-- Prepare work for run %d. [%s]'%(runno,str(daqarea_files),))
      # Create option file
      if with_options:
        num_event_per_file = int(float(self.config.req_event_count)/float(len(files)) + 1.0)
        max_evt = 0
        num_evt = 100
        tot_evt = 0
        for f in used_files: 
          max_evt = max_evt + f[3]
        while tot_evt<self.config.req_event_count and tot_evt<max_evt:
          tot_evt = 0
          for f in used_files:
            if f[3] > num_evt:
              tot_evt = tot_evt + num_evt
            else:
              tot_evt = tot_evt + f[3]
          if tot_evt<self.config.req_event_count:
            num_evt += 100
        log(INFO,'++ Run %d: using refined number of event/file: %d  --> %d [tot evt:%d] [used files:%d]'\
              %(runno,num_event_per_file,num_evt,max_evt,len(used_files),))
        num_event_per_file = num_evt
        self.writeOpts(num_event_per_file,run)

      self.mondb.setRunRunning(run)
      log(INFO,'++ Run %d is prepared for execution: %d [%d] files total. Processing up to %d events/file'%\
            (run,num_file,num_linked,num_evt,))
      return run
    log(WARNING,'++ CANNOT prepare work. [No work to do there...]')
    return None

  # -----------------------------------------------------------------------------
  def prepareRun(self, with_options=False):
    """
    Prepare the next run in the todo list for execution.
    Update the monitoring database corrspondingly.

    \author  M.Frank
    \version 1.0
    """
    run = self.mondb.nextRun(state=self.config.st_todo)
    if run:
      result = self._prepareRun(run, with_options)
      if result: return [result]
    return None

  # -----------------------------------------------------------------------------
  def prepareRuns(self):
    """
    Prepare the a set of compatible runs in the todo list for execution.
    Update the monitoring database corrspondingly.

    \author  M.Frank
    \version 1.0
    """
    prepared_runs = []
    runs = self.mondb.runsByState(state=self.config.st_todo)
    if runs and len(runs):
      module = None
      for run in runs:
        mod = self.runModule(run)
        if module:
          # Test for compatible runs:
          b1 = mod.MooreOnlineVersion != module.MooreOnlineVersion
          b2 = mod.HLTType != module.HLTType
          b3 = False # mod.InitialTCK != module.InitialTCK
          b4 = mod.CondDBTag != module.CondDBTag
          b5 = mod.DDDBTag != module.DDDBTag
          b6 = mod.ConditionsMapping != module.ConditionsMapping
          #print 'mod:', b1, b2, b3, b4, b5, b6,mod.CondDBTag,module.CondDBTag
          if b1 or b2 or b3 or b4 or b5 or b6:
            continue
        else:
          module = mod
        result = self._prepareRun(run)
        if result:
          prepared_runs.append(result)
      if len(prepared_runs)>0:
        return prepared_runs
    return None

  # -----------------------------------------------------------------------------
  def checkRun(self,run,files=None):
    """
    Check if a run finished processing (no more data files).
    Update the monitoring database corrspondingly if the processing finished.

    \author  M.Frank
    \version 1.0
    """
    if run:
      if not files:
        files = os.listdir(self.config.linkDirectory)
      prefix = 'Run_%6d_'%(run,)
      found = False
      for f in files:
        if f.find(prefix) == 0:
          found = True
          break
      if not found:
        log(INFO,'++ Run %s finished.... setting state to DONE.'%(str(run),))
        self.mondb.setRunDone(run)
        return None
      log(DEBUG,'++ Run %s NOT finished.... still %d files to process.'%(str(run),len(files),))
      return run
    return None

  # -----------------------------------------------------------------------------
  def checkRuns(self,runs):
    """
    Check if a list of runs finished processing (no more data files).
    Update the monitoring database corrspondingly if the processing finished.

    \author  M.Frank
    \version 1.0
    """
    if runs:
      running_runs = []
      files = os.listdir(self.config.linkDirectory)
      for run in runs:
        result = self.checkRun(run=run, files=files)
        if result:
          running_runs.append(result)
      if len(running_runs):
        return running_runs
    return None

  def writeOpts(self, num_evts, run, overWrite=True):
    """
    \author  M.Frank
    \version 1.0
    """
    o = 'OnlineEnv.DataDirectory   =  "'+self.config.linkDirectory+'";\n'
    o = 'OnlineEnv.DataDirectories = {"'+self.config.linkDirectory+'"};\n'
    o = o + 'OnlineEnv.EventsPerFile = %d;\n'%(num_evts,)
    if run:
      o = o + 'OnlineEnv.AllowedRuns = ["%d"];\n'%(run,)
    else:
      o = o + 'OnlineEnv.AllowedRuns = [];\n'
    fname = self.config.optsDirectory+os.sep+'ReaderInput.opts'
    acc = os.access(fname,os.O_RDWR)
    if not acc or (acc and overWrite):
      opts = open(fname,'w')
      print >>opts, o
      opts.close()
    o = o.replace(';','').replace('OnlineEnv.','').replace('//','#')
    fname = self.config.optsDirectory+os.sep+'ReaderInput.py'
    acc = os.access(fname,os.O_RDWR)
    if not acc or (acc and overWrite):
      opts = open(fname,'w')
      print >>opts, o
      opts.close()

# -------------------------------------------------------------------------------
class Steer(FSM):
  """
  \author  M.Frank
  \version 1.0
  """

  # -----------------------------------------------------------------------------
  def __init__(self, name, controller, scanner, auto=False):
    """
    \author  M.Frank
    \version 1.0
    """
    FSM.__init__(self, name, partitionid=0, auto=auto)
    self.__active   = False
    self.__working  = True
    self.scanner    = scanner
    self.current_runs = None
    self.controller = Controller(controller,'C')
    self.controller.register(FSM.ST_UNKNOWN, self.onCtrlOffline)
    self.controller.register(FSM.ST_OFFLINE, self.onCtrlOffline)
    self.controller.register(FSM.ST_PAUSED,  self.onCtrlPaused)
    self.controller.register(FSM.ST_RUNNING, self.onCtrlRunning)
    self.controller.register(FSM.ST_READY,   self.onCtrlReady)
    self.controller.register(FSM.ST_ERROR,   self.onCtrlError)
    self.work = IntegerService(scanner.config.announceService,0)
    self.inError = False
  # -----------------------------------------------------------------------------
  def printCtrl(self):
    """
    \author  M.Frank
    \version 1.0
    """
    log(INFO,'State:"%s" Controller:"%s" [Active:%s Working:%s]'%\
        (self.state(),self.controller.state(),str(self.__active),str(self.__working),))
    return self
  # -----------------------------------------------------------------------------
  def setW(self,value):
    """
    \author  M.Frank
    \version 1.0
    """
    self.__working = value
    if not self.current_runs:
      self.__working = False
    return self
  # -----------------------------------------------------------------------------
  def setA(self,value):
    """
    \author  M.Frank
    \version 1.0
    """
    self.__active = value
    return self
  # -----------------------------------------------------------------------------
  def isActive(self):           return self.__active
  # -----------------------------------------------------------------------------
  def onRUNNING(self):          return self.setW(True).setA(True).printCtrl()
  # -----------------------------------------------------------------------------
  def onREADY(self):            return self.setA(False).printCtrl()
  # -----------------------------------------------------------------------------
  def onUNKNOWN(self):          return self.setA(False).printCtrl()
  # -----------------------------------------------------------------------------
  def onCtrlOffline(self):      
    return self.setW(False).setA(True).printCtrl()
  # -----------------------------------------------------------------------------
  def onCtrlReady(self):        return self.printCtrl()
  # -----------------------------------------------------------------------------
  def onCtrlRunning(self):      return self.printCtrl()
  # -----------------------------------------------------------------------------
  def onCtrlPaused(self):       return self.printCtrl()
  # -----------------------------------------------------------------------------
  def onCtrlError(self):
    """
    \author  M.Frank
    \version 1.0
    """
    self.inError=True
    return self.setA(False).printCtrl()
  # -----------------------------------------------------------------------------
  def signalWork(self, runs):
    """
    \author  M.Frank
    \version 1.0
    """
    log(INFO,'Signal new work..... work:%s'%(str(runs),))
    if runs:
      self.work.update(1)
    else:
      self.work.update(0)

  # -----------------------------------------------------------------------------
  def run(self):
    """
    \author  M.Frank
    \version 1.0
    """
    loop = 0
    debug = True
    active_states = [FSM.ST_RUNNING,FSM.ST_READY,FSM.ST_STOPPED]
    scan = self.scanner

    log(INFO,"Starting FSM...")
    self.start()

    scan.writeOpts(0,run=None,overWrite=False)

    self.curr_run = None
    ##if debug: scan.scanDatabase()

    # Check if there is old work left. If yes, restart this run
    if not scan.restart():
      self.__working = False


    start = time.time()
    last_start = time.time()
    while(1):
      log(INFO,'*** Check working:%s active:%s State:%s Ctrl:%s runs:%s'%\
          (str(self.__working),
           str(self.isActive()),
           str(self.state()),
           str(self.controller.state()),
           str(self.curr_run),))

      if self.controller.state() == FSM.ST_OFFLINE or \
         self.controller.state() == FSM.ST_UNKNOWN or \
         self.controller.state() == "":
        if not self.__working:
          loop = loop + 1
          if not self.curr_run:
            #self.curr_run = scan.prepareRuns()
            self.curr_run = scan.prepareRun(with_options=True)
            if not self.curr_run:
              log(INFO,'*** Scanning database. state:%s'%(self.state(),))
              scan.scanDatabase()
              #self.curr_run = scan.prepareRuns()
              self.curr_run = scan.prepareRun(with_options=True)
            if self.curr_run:
              self.signalWork(self.curr_run)
              last_start = time.time()

          # Check run. If done 'self.curr_run' it changes state:
          run = self.curr_run
          if self.inError:
            self.curr_run = None
            self.inError = False
            scan.restart()
            continue
          else:
            self.curr_run = scan.checkRuns(self.curr_run)
          # On state change signal no work
          if run and not self.curr_run:
            self.signalWork(None)
            self.__working = False
            time.sleep(15)
            continue

          #log(INFO,'*** Checkrun:  %s %s %s'%\
          #      (str(self.__working),str(self.isActive()),str(self.curr_run),))

      # If we are out of work for more than 10 hours, exit and restart
      if not self.curr_run:
        diff = time.time()-last_start
        if diff > 10*3600:
          log(INFO,'+'+(132*'='))
          log(INFO,'|')
          log(INFO,'|  Running now for %d hours.'%(int((time.time()-start)/3600.),))
          log(INFO,'|  ...More than %f7.0 hours out of work. Exiting...maybe Oracle is stuck...'%(diff/3600.,))
          log(INFO,'|')
          log(INFO,'+'+(132*'='))
          sys.exit(0)

      time.sleep(150)
      if self.state() == FSM.ST_UNKNOWN:
        log(INFO,'My work is done. Task exit requested....')
        return

# -----------------------------------------------------------------------------
class Ctrl(Controller):
  """
  \author  M.Frank
  \version 1.0
  """
  def __init__(self,cl):
    """
    \author  M.Frank
    \version 1.0
    """
    Controller.__init__(self,cl,'C')
    self.inError = None
    self.register(FSM.ST_ERROR, self.onWorkerError)
    self.register(FSM.ST_OFFLINE, self.onWorkerOutOfError)
    self.register(FSM.ST_NOT_READY, self.onWorkerOutOfError)
  def onWorkerError(self):
    """
    \author  M.Frank
    \version 1.0
    """
    self.inError = time.time()
    print 'Worker in error....',self.name()
    return self
  def onWorkerOutOfError(self):
    """
    \author  M.Frank
    \version 1.0
    """
    self.inError = None
    print 'Worker OK',self.name()
    return self


# -------------------------------------------------------------------------------
class Check:
  """
  \author  M.Frank
  \version 1.0
  """
  def name(self):
    return 'DQChecker'

  # -----------------------------------------------------------------------------
  def __init__(self, name, controller, workers):
    """
    \author  M.Frank
    \version 1.0
    """
    self.workerServices = workers
    self.workers = []
    workers.append(controller)
    for i in workers:
      self.workers.append(Ctrl(i))

  # -----------------------------------------------------------------------------
  def run(self):
    """
    \author  M.Frank
    \version 1.0
    """
    start = time.time()
    while(1):
      err = None
      now = time.time()
      for w in self.workers:
        #print 'Check worker: %s Error: %s'%(w.name(),w.inError,)
        if err and w.inError and err > w.inError:
          err = w.inError
        elif w.inError: err = w.inError

      if err is not None:
        diff = now-err
        log(WARNING,'System in error since %s seconds.'%(str(diff),))
        if diff > 200:
          log(WARNING,'Killing tasks and restarting system.....')
          os.system('/group/online/dataflow/cmtuser/DataQuality/Online/DataQuality/scripts/DQKillTasks.sh')
      if (now - start) > 2*3600:
        log(WARNING,'System running now for %s seconds.....Exit'%(str(now-start),))
        sys.exit(0)
      time.sleep(30)

# -------------------------------------------------------------------------------
def run(run_auto=False):
  """
  \author  M.Frank
  \version 1.0
  """
  import Config, errno

  auto = (len(sys.argv)>1 and sys.argv[1] == '-a') or run_auto
  log(INFO,'%s PID: %-6d  auto: %-5s %s'%(43*'*',os.getpid(), str(auto),43*'*',))
  config = Config.Config()

  try:
    os.makedirs(os.path.abspath(config.linkDirectory))
  except:
    pass
    #print 'Exception: ',str(X)
    #sys.exit(errno.EACCES)

  try:
    os.makedirs(os.path.abspath(config.optsDirectory))
  except Exception,X:
    pass
    #print 'Exception: ',str(X)
    #sys.exit(errno.EACCES)

  os.chdir(os.path.dirname(os.path.abspath(config.linkDirectory)))

  Setup(config).createTables().printSummary()
  run_client = RunFileClient('/HLT/HLT1/Runs','C')
  scanner    = DataQualityScanner(config,run_client)

  log(INFO,"Started scanner...")

  fsm = Steer(os.environ['UTGID'],
              controller=config.statusService,
              scanner=scanner,auto=auto)

  fsm.run()
  sys.exit(0)

# -------------------------------------------------------------------------------
def dump(run_auto, output):
  """
  \author  M.Frank
  \version 1.0
  """
  import Config, errno

  auto = (len(sys.argv)>1 and sys.argv[1] == '-a') or run_auto
  log(INFO,'%s PID: %-6d  auto: %-5s %s'%(43*'*',os.getpid(), str(auto),43*'*',))
  config = Config.Config()

  try:
    os.makedirs(os.path.abspath(config.linkDirectory))
  except:
    pass
    #print 'Exception: ',str(X)
    #sys.exit(errno.EACCES)

  try:
    os.makedirs(os.path.abspath(config.optsDirectory))
  except Exception,X:
    pass
    #print 'Exception: ',str(X)
    #sys.exit(errno.EACCES)

  os.chdir(os.path.dirname(os.path.abspath(config.linkDirectory)))

  Setup(config)
  run_client = RunFileClient('/HLT/HLT1/Runs','C')
  scanner    = DataQualityScanner(config,run_client)

  log(INFO,"Started database dumper...")

  fsm = Steer(os.environ['UTGID'],
              controller=config.statusService,
              scanner=scanner,auto=auto)

  fsm.run()
  sys.exit(0)

# -------------------------------------------------------------------------------
def check():
  """
  \author  M.Frank
  \version 1.0
  """
  import Config, errno
  config = Config.Config()
  log(INFO,'%s Data quality processing-check PID: %-6d %s'%(33*'*',os.getpid(),33*'*',))
  obj = Check(os.environ['UTGID'],
              controller=config.statusService,
              workers=config.workerServices)
  obj.run()
  sys.exit(0)

if __name__=="__main__":
  run()
