"""
     Run Data Quality application in the online environment

     @author M.Frank
"""
__version__ = "$Id: DataQuality.py,v 1.25 2010/11/09 12:20:55 frankb Exp $"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys
import Configurables as Configs
import Gaudi.Configuration as Gaudi
import GaudiKernel
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, RichPIDQCConf
from Configurables import MuEffMonitor, TrackMasterExtrapolator, MuonFastPosTool
from Configurables import CondDB, EventPersistencySvc, HistogramPersistencySvc
from Configurables import EventLoopMgr, LHCb__TransitionSleepSvc
from GaudiKernel.ProcessJobOptions import PrintOff,InstallRootLoggingHandler,logging

#PrintOff(999)
InstallRootLoggingHandler(level=logging.CRITICAL)

GaudiKernel.ProcessJobOptions._parser._parse_units(os.path.expandvars("$STDOPTS/units.opts"))
GaudiKernel.ProcessJobOptions.printing_level=999
requirement = None

def dummy(*args,**kwd): pass

MSG_VERBOSE = 1
MSG_DEBUG   = 2
MSG_INFO    = 3
MSG_WARNING = 4
MSG_ERROR   = 5
MSG_FATAL   = 6
MSG_ALWAYS  = 7

def configureForking(appMgr):
  import OnlineEnv
  from Configurables import LHCb__CheckpointSvc
  numChildren = os.sysconf('SC_NPROCESSORS_ONLN')
  if os.environ.has_key('NBOFSLAVES'):
    numChildren = int(os.environ['NBOFSLAVES'])

  ###print '[ERROR] ++++ configure Brunel for forking with ',numChildren,' children.'
  sys.stdout.flush()
  forker = LHCb__CheckpointSvc("CheckpointSvc")
  forker.NumberOfInstances   = numChildren
  forker.Partition           = OnlineEnv.PartitionName
  forker.TaskType            = os.environ['TASK_TYPE']
  forker.UseCores            = False
  forker.ChildSessions       = False
  forker.FirstChild          = 0
  # Sleep in [ms] for each child in batches of 10:
  forker.ChildSleep          = 50;
  forker.UtgidPattern        = "%UTGID%02d";
  forker.PrintLevel          = 3  # 1=MTCP_DEBUG 2=MTCP_INFO 3=MTCP_WARNING 4=MTCP_ERROR
  forker.OutputLevel         = 4  # 1=VERBOSE 2=DEBUG 3=INFO 4=WARNING 5=ERROR 6=FATAL
  appMgr.ExtSvc.insert(0,forker)
  sys.stdout.flush()

#============================================================================================================
def patchBrunel():
  """
        Instantiate the options to run Brunel with raw data

        @author M.Frank
  """
  import GaudiConf.DstConf
  import Brunel.Configuration
  import OnlineEnv

  #doProtonIon = True

  brunel   = Brunel.Configuration.Brunel()
  onlDBtag = 'fake'

  brunel.OnlineMode = False
  try:
    brunel.DDDBtag    = OnlineEnv.DDDBTag
  except:
    print "DDDBTag not found, use default"

  try:
    brunel.CondDBtag = OnlineEnv.CondDBTag
  except:
    print "CondDBTag not found, use default"

  try:
    onlDBtag = OnlineEnv.OnlDBTag
  except:
    print "[WARN] OnlDBTag not found, use default",onlDBTag


  print '[ERROR]', OnlineEnv.DDDBTag, OnlineEnv.CondDBTag, onlDBtag 


  conddb = CondDB()
  conddb.IgnoreHeartBeat = True

  #
  # Use Roel fake DB at startup
  #
  conddb.Tags["ONLINE"]      = onlDBtag
  conddb.EnableRunStampCheck = False
  conddb.IgnoreHeartBeat     = True
  conddb.UseDBSnapshot       = True

  #  from Configurables import EventClockSvc
  # EventClockSvc(InitialTime = 1465516800000000000)
  #  EventClockSvc(InitialTime = 1477267200000000000)


  #
  # Adjust to pickup the proper online conditions
  #
  import ConditionsMap
  conddb.Online = True

  brunel.DataType      = "2017"
  brunel.UseDBSnapshot = True  # Try it
  brunel.WriteFSR      = False # This crashes Jaap's stuff

  brunel.Histograms    = "OfflineFull"
  brunel.Context       = "Offline"
  brunel.MainSequence  = ['ProcessPhase/Reco', 'ProcessPhase/Moni']

  # Hack to disable old RICH monitoring
  #from Configurables import RecMoniConf, RecSysConf

  #richSeq                     = "RICHFUTURE"
  #brunel.RichSequences        = [richSeq]
  #RecSysConf().RichSequences  = [richSeq]
  #RecMoniConf().RichSequences = [richSeq]

  #if richSeq == "RICHFUTURE" :
  #  if "RICH" in RecMoniConf().MoniSequence:
  #    RecMoniConf().MoniSequence.remove('RICH')

  # The CondDB configurable only configures the RunChangeHandler
  # if UseDBSnapshot is set to True, so if it isn't, we do it
  # ourselves
  if not brunel.UseDBSnapshot:
    from Configurables import ApplicationMgr
    from Configurables import RunChangeHandlerSvc
    baseloc = brunel.getProp('DBSnapshotDirectory')
    rch = RunChangeHandlerSvc()
    rch.Conditions = dict( (c,'/'.join([baseloc,f])) for f,cs in ConditionsMap.RunChangeHandlerConditions.iteritems() for c in cs )
    ApplicationMgr().ExtSvc.append(rch)
  #
  # Adjust to pickup the proper online conditions from ConditionsMap
  #
  conddb.setProp('RunChangeHandlerConditions', ConditionsMap.RunChangeHandlerConditions)
  conddb.setProp('EnableRunChangeHandler', True)

  # Enabled data-on-demand
  Gaudi.ApplicationMgr().ExtSvc += [ "DataOnDemandSvc" ]

  
  ################################################################################
  # Option file for reconstrucion of non proton-proton data.
  # These GECS (in TrackSys and RichRecSysConf) will essentially
  # disable the application of Global Event Cuts in the track reconstruction
  # and Rich reconstruction.  
  ################################################################################

  if not OnlineEnv.DataType == "COLLISION18":
    from Configurables import TrackSys
    TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }
    
    #from Configurables import RichRecSysConf
    #rConf = RichRecSysConf("RichOfflineRec")
    
    #rConf.pixelConfig().MaxPixels      = 90000
    #rConf.trackConfig().MaxInputTracks = 99999
    #rConf.trackConfig().MaxUsedTracks  = 10000  # this used to be 1000
    #rConf.photonConfig().MaxPhotons    = 900000 # this used to be 250000
    #rConf.gpidConfig().MaxUsedPixels   = 120000 # this used to be 30000
    #rConf.gpidConfig().MaxUsedTracks   = 10000  # this used to be 1000

  ################################################################################
  #                                                                              #
  # The sequencer to run all the monitoring in                                   #
  #                                                                              #
  ################################################################################

  DQMoniSeq = GaudiSequencer("DQMoniSeq")
  DQMoniSeq.IgnoreFilterPassed = True

  ################################################################################
  #                                                                              #
  # Configure the tesla online vs. offline comparison                            #
  #                                                                              #
  ################################################################################

  TeslaMainSeq = GaudiSequencer("TeslaMainSeq")
  
  #
  # Run Tesla to produce physics objects from Trigger banks
  #
  
  TeslaSeq = GaudiSequencer("TeslaSeq")
  TeslaSeq.IgnoreFilterPassed   = True
  #MAd   
  #MAd    from DAQSys.Decoders import DecoderDB
  #MAd   
  #MAd    Hlt1VertexReportsDecoder = DecoderDB["HltVertexReportsDecoder/Hlt1VertexReportsDecoder"].setup()
  #MAd    Hlt2VertexReportsDecoder = DecoderDB["HltVertexReportsDecoder/Hlt2VertexReportsDecoder"].setup()
  #MAd    Hlt2SelReportsDecoder    = DecoderDB["HltSelReportsDecoder/Hlt2SelReportsDecoder"].setup()
  #MAd    
  #MAd    TeslaSeq.Members += [Hlt1VertexReportsDecoder,
  #MAd                         Hlt2VertexReportsDecoder,
  #MAd                         Hlt2SelReportsDecoder]
  #MAd    
  
  linesToResurrect = [
    "Hlt2PIDD02KPiTagTurboCalib"
    , "Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib"
    , "Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib"
    , "Hlt2PIDLambda2PPiLLTurboCalib"
    , "Hlt2PIDLambda2PPiLLhighPTTurboCalib"
    , "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib"
    , "Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib"
    , "Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib"
    , "Hlt2PIDDs2PiPhiKKUnbiasedTurboCalib"
  ]
  
  from DAQSys.Decoders     import DecoderDB
  from DAQSys.DecoderClass import Decoder
  from GaudiConf           import PersistRecoConf
  from Configurables       import HltPackedDataDecoder
  from Configurables       import UnpackParticlesAndVertices

  
  decoder = DecoderDB.pop("HltPackedDataDecoder/Hlt2PackedDataDecoder")
  assert not decoder.wasUsed(), ('Hlt2PackedDataDecoder was '
                                 'aready setup, cannot remove!')
  
  packing = PersistRecoConf.PersistRecoPacking('2018')
  decoder = Decoder(
    "HltPackedDataDecoder/Hlt2PackedDataDecoder",
    active=True, banks=["DstData"],
    inputs={"RawEventLocations": None},
    outputs=packing.packedLocations(),
    properties={"ContainerMap":
                packing.packedToOutputLocationMap()},
    conf=DecoderDB
  )
  decoder.setup()
  
  DecoderSeq = GaudiSequencer("DecodeSeq")
  
  packed_data_decoder = HltPackedDataDecoder('Hlt2PackedDataDecoder')
  DecoderSeq.Members.append(packed_data_decoder)

  prpacking   = PersistRecoConf.PersistRecoPacking('2018')
  prunpackers = prpacking.unpackers()
  
  UnpackerSeq = GaudiSequencer('TeslaUnpackers')
  # Need to run the packers in reverse order, due to object dependencies
  UnpackerSeq.Members += prunpackers[::-1]
  unpack_psandvs = UnpackParticlesAndVertices(
    InputStream='/Event/Turbo'
  )
  UnpackerSeq.Members.append(unpack_psandvs)

  DQMoniSeq.Members += [DecoderSeq]
  DQMoniSeq.Members += [UnpackerSeq]

  
  from Configurables import TeslaReportAlgo
  
  #for line in linesToResurrect:
  #  trig = TeslaReportAlgo("TslR" + line)
  #  trig.OutputPrefix = "Turbo/"
  #  trig.PV           = "Offline"
  #  trig.VertRepLoc   = "Hlt2/VertexReports"
  #  trig.PVLoc        = "/Event/Turbo/Primary"
  #  trig.TriggerLine  = line
  
  #  TeslaSeq.Members += [trig]
  
  trig = TeslaReportAlgo("TslR")
  trig.OutputPrefix = "Turbo/"
  trig.PV           = "Offline"
  trig.VertRepLoc   = "Hlt2/VertexReports"
  trig.PVLoc        = "/Event/Turbo/Primary"
  trig.TriggerLines  = linesToResurrect
   
  #MAd TeslaSeq.Members += [trig]
  
  #
  # Tesla monitor: read Tesla objects and produce plots
  #
  
  teslaCut = "(MAXTREE(TRGHP, ISBASIC) < 0.3)"
  
  from TeslaTools.TeslaMonitor import TeslaH1
  
  TeslaSeq.Members += [
    TeslaH1("Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:CHILD(M,1)",
            ";m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
               1000, 1865 - 60, 1865 + 60, teslaCut).getAlgorithm()
    , TeslaH1("Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:M",
              ";m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 1865 + 139 - 60, 1865 + 155 + 60, teslaCut).getAlgorithm()
    , TeslaH1("Hlt2PIDD02KPiTagTurboCalib:D*(2010)+:M-CHILD(M,1)",
              ";m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}) [MeV/c^{2}]; Candidates",
              1000, 139, 155, teslaCut).getAlgorithm()
    , TeslaH1("Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib:J/psi(1S):M" ,
              ";m(#mu#mu) [MeV/c^{2}]; Candidates",
              1000, 3096 - 195, 3096 + 195, teslaCut).getAlgorithm()
    , TeslaH1("Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib:J/psi(1S):M" ,
               ";m(#mu#mu) [MeV/c^{2}]; Candidates",
               1000, 3096 - 195, 3096 + 195, teslaCut).getAlgorithm()
    , TeslaH1("Hlt2PIDLambda2PPiLLTurboCalib:Lambda0:M" ,
              ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1100, 1140, teslaCut).getAlgorithm()
    , TeslaH1("Hlt2PIDLambda2PPiLLhighPTTurboCalib:Lambda0:M" ,
              ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1100, 1140, teslaCut).getAlgorithm()
    , TeslaH1("Hlt2PIDLambda2PPiLLveryhighPTTurboCalib:Lambda0:M" ,
              ";m(p#pi^{-}) [MeV/c^{2}]; Candidates",
              1000, 1100, 1140, teslaCut).getAlgorithm()
  ]
  
  #
  # TeslaBrunel Monitors: compare Brunel and Tesla reconstruction
  #
  
  from TeslaTools.TeslaMonitor import TeslaBrunelMonitorConf as TBM
  
  variables = {"PT"                 : ["PT",                          10]     
               , "P"                : ["P",                          100]     
               , "ETA"              : ["ETA" ,                       0.1]    
               , "PIDK"             : ["PIDK",                        10]  
               , "PIDmu"            : ["PIDmu",                       10]
               , "PIDp"             : ["PIDp",                        10]
               , "PIDe"             : ["PIDe",                        10]
               , "MINIPCHI2"        : ["BPVIPCHI2()",                100]
               , "MINIP"            : ["BPVIP()",                      1]
               , "RichDLLe"         : ["PPFUN(PP_RichDLLe)",          10]
               , "RichDLLpi"        : ["PPFUN(PP_RichDLLpi)",         10]
               , "RichDLLmu"        : ["PPFUN(PP_RichDLLmu)",         10]
               , "RichDLLk"         : ["PPFUN(PP_RichDLLk)",          10]
               , "RichDLLp"         : ["PPFUN(PP_RichDLLp)",          10]
               , "RichDLLbt"        : ["PPFUN(PP_RichDLLbt)",         10]
               , "MuonMuLL"         : ["PPFUN(PP_MuonMuLL)",          10]
               , "MuonBgLL"         : ["PPFUN(PP_MuonBkgLL)",         10]
               , "MuonNShared"      : ["PPFUN(PP_MuonNShared)",       10]
               , "MuonPIDStatus"    : ["PPFUN(PP_MuonPIDStatus)",     10]
               , "EcalPIDe"         : ["PPFUN(PP_EcalPIDe)",          10]
               , "EcalPIDmu"        : ["PPFUN(PP_EcalPIDmu)",         10]
               , "HcalPIDe"         : ["PPFUN(PP_HcalPIDe)",          10]
               , "HcalPIDmu"        : ["PPFUN(PP_HcalPIDmu)",         10]
               , "PrsPIDe"          : ["PPFUN(PP_PrsPIDe)",           10]
               , "BremPIDe"         : ["PPFUN(PP_BremPIDe)",          10]
               , "InAccBrem"        : ["PPFUN(PP_InAccBrem)",        1.5]
               , "InAccMuon"        : ["PPFUN(PP_InAccMuon)",        1.5]
               , "InAccSpd"         : ["PPFUN(PP_InAccSpd )",        1.5]
               , "InAccEcal"        : ["PPFUN(PP_InAccEcal)",        1.5]
               , "InAccHcal"        : ["PPFUN(PP_InAccHcal)",        1.5]
               , "InAccHcal"        : ["PPFUN(PP_InAccHcal)",        1.5]
               , "CaloTrMatch"      : ["PPFUN(PP_CaloTrMatch)",       10]
               , "CalElectronMatch" : ["PPFUN(PP_CaloElectronMatch)", 10]
               , "CaloBremMatch"    : ["PPFUN(PP_CaloBremMatch)",     10]
               , "CaloChargedSpd"   : ["PPFUN(PP_CaloChargedSpd)",    10]
               , "CaloChargedPrs"   : ["PPFUN(PP_CaloChargedPrs) ",   10]
               , "CaloChargedEcal"  : ["PPFUN(PP_CaloChargedEcal) ",  10]
               , "CaloDepositID"    : ["PPFUN(PP_CaloDepositID) ",    10]
               , "ShowerShape"      : ["PPFUN(PP_ShowerShape) ",      10]
               , "ClusterMass"      : ["PPFUN(PP_ClusterMass) ",      10]
               , "CaloNeutralSpd"   : ["PPFUN(PP_CaloNeutralSpd) ",   10]
               , "CaloNeutralPrs"   : ["PPFUN(PP_CaloNeutralPrs) ",   10]
               , "CaloNeutralEcal"  : ["PPFUN(PP_CaloNeutralEcal) ",  10]
               , "CaloSpdE"         : ["PPFUN(PP_CaloSpdE) ",         10]
               , "CaloPrsE"         : ["PPFUN(PP_CaloPrsE) ",         10]
               , "CaloEcalE"        : ["PPFUN(PP_CaloEcalE) ",        10]
               , "CaloHcalE"        : ["PPFUN(PP_CaloHcalE) ",        10]
               , "CaloEcalChi2"     : ["PPFUN(PP_CaloEcalChi2) ",     10]
               , "CaloBremChi2"     : ["PPFUN(PP_CaloBremChi2) ",     10]
               , "CaloClusChi2"     : ["PPFUN(PP_CaloClusChi2) ",     10]
               , "CaloTrajectoryL"  : ["PPFUN(PP_CaloTrajectoryL) ",  10]
               , "EcalPIDe"         : ["PPFUN(PP_EcalPIDe) ",         10]
               , "PrsPIDe"          : ["PPFUN(PP_PrsPIDe) ",          10]
               , "HcalPIDe"         : ["PPFUN(PP_HcalPIDe) ",         10]
               , "EcalPIDmu"        : ["PPFUN(PP_EcalPIDmu) ",        10]
               , "HcalPIDmu"        : ["PPFUN(PP_HcalPIDmu) ",        10]
               , "PhotonID"         : ["PPFUN(PP_PhotonID) ",         10]
               , "TrackChi2PerDof"  : ["PPFUN(PP_TrackChi2PerDof) ",  10]
               , "TrackNumDof"      : ["PPFUN(PP_TrackNumDof) ",      10]
  }
  
  tbmP  = TBM('TBMonitorProton', HistName="TBMp",  TurboInput='Hlt2PIDLambda2PPiLLTurboCalib')
  tbmK  = TBM('TBMonitorKaon',   HistName="TBMk",  TurboInput='Hlt2PIDD02KPiTagTurboCalib')
  tbmPi = TBM('TBMonitorPion',   HistName="TBMpi", TurboInput='Hlt2PIDD02KPiTagTurboCalib')
  tbmMu = TBM('TBMonitorMuon',   HistName="TBMmu", TurboInput='Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib')
  
  for var in variables:
    tbmP.addVariable("P_"+var, particle="p+", 
                     code=variables[var][0], 
                     max =variables[var][1], 
                     cut=teslaCut)
    tbmK.addVariable("K_"+var, particle="K+", 
                     code=variables[var][0], 
                     max =variables[var][1], 
                     cut=teslaCut)
    tbmPi.addVariable("Pi_"+var, particle="pi+", 
                      code=variables[var][0], 
                      max =variables[var][1], 
                      cut=teslaCut)
    tbmMu.addVariable("Mu_"+var, particle="mu+", 
                      code=variables[var][0], 
                      max =variables[var][1], 
                      cut=teslaCut + " & (ISMUON)")
  
    
  TeslaSeq.Members += [tbm.getAlgorithm() 
                       for tbm in [tbmP, tbmK, tbmPi, tbmMu]]
  TeslaSeq.Members += [tbm.getAlgorithm() 
                       for tbm in [tbmP, tbmK, tbmPi]]
  
  #
  # ReadHltReport
  #
  
#MAd   from Configurables import ReadHltReport
#MAd   TeslaSeq.Members +=  [  
#MAd     ReadHltReport(HltDecReportsLocation = "Hlt2/DecReports", 
#MAd                   HltSelReportsLocation = "Hlt2/SelReports")]
#MAd   

  from PhysConf.Filters import LoKi_Filters
  fltrs = LoKi_Filters (
    HLT2_Code     = " HLT_PASS_RE('Hlt2.*TurboCalibDecision')" ,
    #    STRIP_Code = " HLT_PASS_RE('StrippingStripD2K(K|P)P_(A|B)_LoosePID_Sig.*Decision') ",
    #    VOID_Code  = " EXISTS ('/Event/Strip') & EXISTS('/Event/Charm') "
  )

  TeslaMainSeq.Members = ([fltrs.sequence("RecoFilter")] + [TeslaSeq])
  
  DQMoniSeq.Members += [TeslaMainSeq]
  
  ################################################################################
  #                                                                              #
  # Set up PID monitoring sequence                                               #
  #                                                                              #
  ################################################################################

  RichPidSeq = GaudiSequencer("RichPIDSelections")

  brunel.setOtherProps( RichPIDQCConf(), ['OutputLevel','Context'] )
  RichPIDQCConf().setProp("CalibSequencer", RichPidSeq)
  RichPIDQCConf().Candidates["KshortPiPi"] = []

  if OnlineEnv.DataType == "LEAD18":
    RichPIDQCConf().Candidates["JPsiMuMu"]   = []
    RichPIDQCConf().Candidates["DstarD0Pi"]  = []
    RichPIDQCConf().Candidates["LambdaPrPi"] = []
    RichPIDQCConf().Candidates["DsPhiPi"]    = []

  DQMoniSeq.Members += [RichPidSeq]

  ################################################################################
  #                                                                              #
  # Configure the muon efficiency monitor                                        #
  #                                                                              #
  ################################################################################

  muEffMoni = MuEffMonitor("MuEffMonitor")

  muEffMoni.addTool(TrackMasterExtrapolator, name = "MuEffExtrap")
  muEffMoni.Extrapolator = muEffMoni.MuEffExtrap

  muEffMoni.MuEffExtrap.ApplyMultScattCorr  = True
  muEffMoni.MuEffExtrap.ApplyEnergyLossCorr = True
  muEffMoni.MuEffExtrap.MaterialLocator     = "SimplifiedMaterialLocator" 
  muEffMoni.MuEffExtrap.OutputLevel         = 6

  muEffMoni.nSigma1X = [11., 8., 7., 7.]
  muEffMoni.nSigma1Y = [ 6., 5., 5., 5.]
  muEffMoni.nSigmaX  = [ 5., 5., 5., 5.]
  muEffMoni.nSigmaY  = [ 5., 5., 5., 5.]

  muEffMoni.RequiredStations = 4
  muEffMoni.MomentumCut      = 3000.0
  muEffMoni.nSigmaFidVol     = 3.0

  muEffMoni.UseCalo  = True
  muEffMoni.EecalMax = 1500.0
  muEffMoni.EhcalMax = 5000.0
  muEffMoni.EhcalMin = 1000.0
        
  muEffMoni.Chi2ProbTrMin =  0.01
  muEffMoni.Chi2MuMin     = 10.0
  muEffMoni.nSigmaXother  =  2.0
  muEffMoni.nSigmaYother  =  2.0
  
  muEffMoni.HistoLevel = "OfflineFull"

  DQMoniSeq.Members += [muEffMoni]
  
  ################################################################################
  #                                                                              #
  # Configure the muon pid monitor                                               #
  #                                                                              #
  ################################################################################
  
  from Configurables import MuIDMonitor

  MuPidSeq = GaudiSequencer("MuPidSeq")
  MuPidSeq.IgnoreFilterPassed = True

  MuPidMoniSeq_Lambda = GaudiSequencer("MuPidMoniSeq_Lambda")
  MuPidMoniSeq_Jpsi   = GaudiSequencer("MuPidMoniSeq_Jpsi")
  
  #
  # Monitoring muon mis-id with Lambda_0
  #
  
  MuIDLambdaPlot             = MuIDMonitor("MuIDLambdaPlot")
  MuIDLambdaPlot.Inputs      = [ "/Event/Turbo/Hlt2PIDLambda2PPiLLTurboCalib/Particles",
                                 "/Event/Turbo/Hlt2PIDLambda2PPiLLhighPTTurboCalib/Particles",
                                 "/Event/Turbo/Hlt2PIDLambda2PPiLLveryhighPTTurboCalib/Particles"]
  MuIDLambdaPlot.OutputLevel = 6

  MuIDLambdaPlot.MassMean       = 1115.68
  MuIDLambdaPlot.MassWindow     =   20.
  MuIDLambdaPlot.EffMassWin     =    2.;
  
  MuIDLambdaPlot.JpsiAnalysis   = 0
  MuIDLambdaPlot.LambdaAnalysis = 1
  MuIDLambdaPlot.HitInFoi       = 1
  
  
  MuIDLambdaPlot.PreSelMomentum = 3000.              # MuonID preselection momentum (MeV/c)
  MuIDLambdaPlot.MomentumCuts   = [ 6000. , 10000. ] # MuonID momentum cut ranges (MeV/c)
  
  # MuonID FOI parameters
  
  MuIDLambdaPlot.FOIfactor      = 1.
  
  MuIDLambdaPlot.XFOIParameter1 = [5.5, 4.0, 3.3, 2.8,
                                   5.2, 3.6, 2.4, 2.4,
                                   5.7, 4.4, 2.8, 2.3,
                                   5.1, 3.1, 2.3, 2.1,
                                   5.8, 3.4, 2.6, 2.8]

  MuIDLambdaPlot.XFOIParameter2 = [11., 3., 1., 1.,
                                   31., 28., 21., 17.,
                                   30., 31., 27., 22.,
                                   28., 33., 35., 47.,
                                   31., 39., 56., 151.]

  MuIDLambdaPlot.XFOIParameter3 = [0.20, 0.08, 0.03, 0.1,
                                   0.06, 0.08, 0.10, 0.15,
                                   0.04, 0.06, 0.09, 0.12,
                                   0.08, 0.15, 0.23, 0.36,
                                   0.07, 0.14, 0.24, 0.49]
 
  MuIDLambdaPlot.YFOIParameter1 = [2.8, 1.7, -153., 1.9,
                                   3.3, 2.1, 1.7, 1.6,
                                   3.6, 2.8, 1.9, 1.8,
                                   4.4, 3.3, 2.2, 2.2,
                                   4.8, 3.9, 2.6, 2.3]
  
  MuIDLambdaPlot.YFOIParameter2 = [3., 2., 156., 0.,
                                   17., 15., 9., 5.,
                                   26., 25., 16., 15.,
                                   30., 49., 57., 92.,
                                   32., 55., 96., 166.]
  
  MuIDLambdaPlot.YFOIParameter3 = [0.03, 0.02, 0.00, 0.09,
                                   0.13, 0.19, 0.19, 0.24, 
                                   0.11, 0.19, 0.21, 0.32,
                                   0.10, 0.22, 0.30, 0.52,
                                   0.08, 0.20, 0.34, 0.52]


  #
  # Parameters of the Landau functions
  #
  
  MuIDLambdaPlot.distMuon  = [0.311, 1.349, 0.524, 0.0020, 17., 10.6, 0.04, 4.1, 1.64]
  MuIDLambdaPlot.distPion  = [11., -12., 0.2029, -0.026, 0.06, 0.59, 0.008, -29., 41.]
  
  MuPidMoniSeq_Lambda.Members += [MuIDLambdaPlot]
  
  #
  # Monitoring muon id with J/psi
  #
  
  MuIDJpsiPlot             = MuIDMonitor("MuIDJpsiPlot")
  MuIDJpsiPlot.Inputs      = [ "/Event/Turbo/Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib/Particles",
                               "/Event/Turbo/Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib/Particles" ]
  MuIDJpsiPlot.OutputLevel = 6
  
  MuIDJpsiPlot.MassMean   = 3096.91
  MuIDJpsiPlot.MassWindow = 300.
  MuIDJpsiPlot.EffMassWin = 20.;
  
  MuIDJpsiPlot.JpsiAnalysis   = 1
  MuIDJpsiPlot.LambdaAnalysis = 0
  MuIDJpsiPlot.HitInFoi       = 1
  
  MuIDJpsiPlot.PreSelMomentum = MuIDLambdaPlot.PreSelMomentum
  MuIDJpsiPlot.MomentumCuts   = MuIDLambdaPlot.MomentumCuts
  
  # MuonID FOI parameters
  
  MuIDJpsiPlot.FOIfactor      = MuIDLambdaPlot.FOIfactor
  MuIDJpsiPlot.XFOIParameter1 = MuIDLambdaPlot.XFOIParameter1
  MuIDJpsiPlot.XFOIParameter2 = MuIDLambdaPlot.XFOIParameter2
  MuIDJpsiPlot.XFOIParameter3 = MuIDLambdaPlot.XFOIParameter3
  MuIDJpsiPlot.YFOIParameter1 = MuIDLambdaPlot.YFOIParameter1
  MuIDJpsiPlot.YFOIParameter2 = MuIDLambdaPlot.YFOIParameter2
  MuIDJpsiPlot.YFOIParameter3 = MuIDLambdaPlot.YFOIParameter3
  
  #
  # Parameters of the Landau functions
  #
  
  MuIDJpsiPlot.distMuon = MuIDLambdaPlot.distMuon
  MuIDJpsiPlot.distPion = MuIDLambdaPlot.distPion
  
  MuPidMoniSeq_Jpsi.Members += [MuIDJpsiPlot]
  
  MuPidSeq.Members += [MuPidMoniSeq_Lambda, MuPidMoniSeq_Jpsi]

  #
  # Check PV exists before making J/psi or Lambda
  # Needed to remove lots of Loki warnings.
  #

  from Configurables import CheckPV
  checkPV_1 = CheckPV('CheckMin1PV_1', MinPVs=1)

  MuPidAndCheckPVSeq = GaudiSequencer("MuPidAndCheckPVSeq")
  MuPidAndCheckPVSeq.Members += [checkPV_1, MuPidSeq]

  DQMoniSeq.Members += [MuPidAndCheckPVSeq]
  ################################################################################
  #                                                                              #
  # Make D* -> D0 pi plots                                                       #
  #                                                                              #
  ################################################################################
  from Configurables          import CombineParticles
  from Configurables          import FilterDesktop
  from StandardParticles      import StdLoosePions, StdLooseKaons, StdAllNoPIDsPions
  from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand


  MonitorDstarSeq = GaudiSequencer("MonitorDstar")

  #
  # Filter for the D0 pion and kaon and for the D* slow pion.
  #
  
  DstKaonFCut     = "(TRCHI2DOF<3.0) & (PT>200*MeV) & (P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0) & (PIDK>5.0)"
  DstPionFCut     = "(TRCHI2DOF<3.0) & (PT>200*MeV) & (P>1000*MeV) & (MIPCHI2DV(PRIMARY)>4.0) & (PIDK<5.0)"
  DstSlowPionFCut = "(TRCHI2DOF<3.0) & (PT>100*MeV) & (P>1000*MeV)"
  
  FilterDstPions          = FilterDesktop("FilterDstPions")
  FilterDstPions.Code     = DstPionFCut
  FilterDstPionsSel       = Selection("FilterDstPionsSel",
                                      Algorithm          = FilterDstPions,
                                      RequiredSelections = [StdLoosePions])
  
  FilterDstKaons          = FilterDesktop("FilterDstKaons")
  FilterDstKaons.Code     = DstKaonFCut
  FilterDstKaonsSel       = Selection("FilterDstKaonsSel",
                                      Algorithm          = FilterDstKaons,
                                      RequiredSelections = [StdLooseKaons])

  FilterDstSlowPions      = FilterDesktop("FilterDstSlowPions")
  FilterDstSlowPions.Code = DstSlowPionFCut
  FilterDstSlowPionsSel   = Selection("FilterDstSlowPionsSel",
                                      Algorithm          = FilterDstSlowPions,
                                      RequiredSelections = [StdAllNoPIDsPions])
  
  #
  # Make the D0
  #

  DstKaonCut     = "(PT>250*MeV) & (P>2000*MeV) & (MIPCHI2DV(PRIMARY)>16.0)"
  DstPionCut     = "(PT>250*MeV) & (P>2000*MeV) & (MIPCHI2DV(PRIMARY)>16.0)"
  
  DstD0CombCut   = "in_range(1775.0*MeV,AM,1955.0*MeV) & ((APT1>500.0*MeV) | (APT2>500.0*MeV)) & (AMINDOCA('')<0.1*mm)"
  DstD0MotherCut = "(VFASPF(VCHI2PDOF)<10.0) & (BPVVDCHI2>49.0) & (BPVDIRA>0.99985)"
  
  D0ForDst                 = CombineParticles("D0ForDst")
  D0ForDst.DecayDescriptor = "[D0 -> K- pi+]cc"
  D0ForDst.CombinationCut  = DstD0CombCut
  D0ForDst.MotherCut       = DstD0MotherCut
  D0ForDst.DaughtersCuts   = {"pi+" : DstPionCut,
                              "K-"  : DstKaonCut}
  
  D0ForDstSel = Selection("D0ForDstSel",
                          Algorithm = D0ForDst,
                          RequiredSelections = [FilterDstPionsSel, FilterDstKaonsSel])
  D0ForDstSeq = SelectionSequence("D0ForDstSeq",
                                  TopSelection = D0ForDstSel)
  
  #
  # Make the D*
  #
  
  DstCombCut   = "in_range(130.0*MeV,(AM-AM1),165*MeV)"
  DstMotherCut = "(VFASPF(VCHI2PDOF)<25.0) & in_range(130.0*MeV,(M-M1),160.0*MeV)"

  MakeDst                 = CombineParticles("MakeDst")
  MakeDst.DecayDescriptor = "[D*(2010)+ -> D0 pi+]cc"
  MakeDst.CombinationCut  = DstCombCut
  MakeDst.MotherCut       = DstMotherCut
  
  #
  # Add the plots
  #

  import GaudiKernel.SystemOfUnits as Units
  from   Configurables import LoKi__Hybrid__PlotTool as PlotTool
  
  MakeDst.HistoProduce = True
  
  MakeDst.addTool(PlotTool("DaughtersPlots"))
  MakeDst.DaughtersPlots.Histos = {"P/1000"  : ('%1% momentum',0,100) ,
                                   "PT/1000" : ('%1% pt',0,5,100) ,
                                   "M"       : ('Mass(%1%)',1755.0*Units.MeV,1955.0*Units.MeV)}
  MakeDst.addTool(PlotTool("MotherPlots"))
  MakeDst.MotherPlots.Histos = {"P/1000"  : ('%1% momentum',0,100) ,
                                "PT/1000" : ('%1% pt',0,5,100) ,
                                "M"       : ('Mass',1910.0*Units.MeV,2110.0*Units.MeV),
                                "M-M1"    : ('#Delta Mass',130.0*Units.MeV,160.0*Units.MeV)}
  
  MakeDstSel = Selection("MakeDstSel",
                         Algorithm = MakeDst,
                         RequiredSelections = [FilterDstSlowPionsSel, D0ForDstSel])
  MakeDstSeq = SelectionSequence("MakeDstSeq",
                                 TopSelection = MakeDstSel)

  checkPV_2 = CheckPV('CheckMin1PV_2', MinPVs=1)
  MonitorDstarSeq.Members += [checkPV_2, MakeDstSeq.sequence()]

  DQMoniSeq.Members += [MonitorDstarSeq]

  ################################################################################
  #                                                                              #
  # Make J/psi -> mu+ mu- plots                                                  #
  #                                                                              #
  ################################################################################

  MonitorJpsiSeq = GaudiSequencer("MonitorJpsi")

  #
  # Filter the muons
  #
  
  from StandardParticles import StdAllLooseMuons
  
  JpsiMuonCut = "(PT>650*MeV) & (P>3*GeV) & (P<500*GeV)"
  
  FilterJpsiMuons      = FilterDesktop("FilterJpsiMuons")
  FilterJpsiMuons.Code = JpsiMuonCut
  FilterJpsiMuonsSel   = Selection("FilterJpsiMuonsSel",
                                   Algorithm          = FilterJpsiMuons,
                                   RequiredSelections = [StdAllLooseMuons]
                                   )

  #
  # Make the J/psi
  #
  
  JpsiDQCombCut   = "(ADAMASS('J/psi(1S)')<300*MeV)"
  JpsiDQMotherCut = "in_range(3000.0*MeV,M,3200.0*MeV) & (VFASPF(VCHI2PDOF)<25)"

  JpsiDQMonitor                 = CombineParticles("JpsiDQMonitor")
  JpsiDQMonitor.DecayDescriptor = "J/psi(1S) -> mu+ mu-"
  JpsiDQMonitor.CombinationCut  = JpsiDQCombCut
  JpsiDQMonitor.MotherCut       = JpsiDQMotherCut
  
  JpsiDQMonitorSel = Selection("JpsiDQMonitorSel",
                               Algorithm = JpsiDQMonitor,
                               RequiredSelections = [StdAllLooseMuons])
  JpsiDQMonitorSeq = SelectionSequence("JpsiDQMonitorSeq",
                                       TopSelection = JpsiDQMonitorSel)
  
  from Configurables import ParticleMonitor
  JpsiDQPlotter        = ParticleMonitor("JpsiDQMonitorPlots")
  JpsiDQPlotter.Inputs = [JpsiDQMonitorSel.outputLocation()]

  MonitorJpsiSeq.Members += [JpsiDQMonitorSeq.sequence(), JpsiDQPlotter]
  
  DQMoniSeq.Members += [MonitorJpsiSeq]

  ################################################################################
  #                                                                              #
  # Make L0Muon asymmetry plots                                                  #
  #                                                                              #
  ################################################################################
  MonitorL0MuonSeq = GaudiSequencer("MonitorL0Muon")

  from PhysConf.Filters import LoKi_Filters
  l0Filter = LoKi_Filters(L0DU_Code = "L0_CHANNEL('Muon')")
  from L0DU.L0Algs import decodeL0Muon
  l0muondecode = decodeL0Muon()
  l0muondecode.DAQMode = 1
  from Gaudi.Configuration import DEBUG

  import os
  
  from Configurables import L0MuonAsymMonitor
  L0MuonDQAsym = L0MuonAsymMonitor("L0MuonDQAsym")
  L0MuonDQAsym.Online               = True
  L0MuonDQAsym.Input                = JpsiDQMonitorSel.outputLocation()
  L0MuonDQAsym.L0MuonADC2MeV        = 50. # ADC -> MeV conversion factor 
  L0MuonDQAsym.L0MuonPtADCThreshold = -1  # Use the threshold defined in the TCK
  # L0MuonDQAsym can recompute PT and asymmetry with different inputs :
  # Case 1 : Recompute PT with old LUT (V10), keep default threshold (TCK 0x1703)
  L0MuonDQAsym.L0MuonM1M2LUT        = os.path.join(os.environ['PARAMFILESROOT'] , 'data' , 'L0MuonM1M2LUT_V10')
  L0MuonDQAsym.L0MuonPtADCCutOnTIS  = 14  # (ADC), other threshold used to compute a charge asymmetry
  # Case 2  : Do not recompute PT, apply new threshold
  L0MuonDQAsym.L0MuonM1M2LUT2       = ""  # 
  L0MuonDQAsym.L0MuonPtADCCutOnTIS2 = 18  # (ADC), yet another threshold used to compute a charge asymmetry
  # Write out
  L0MuonDQAsym.FillLUT              = False # Write out data to compute new LUT
  L0MuonDQAsym.FillLUTDirectory     = "" # "/group/trg/l0muon/ParamFiles/online"
  
  MonitorL0MuonSeq.Members += [l0Filter.sequence("L0FilterForL0MuonDQ"),l0muondecode,L0MuonDQAsym]
  
  DQMoniSeq.Members += [MonitorL0MuonSeq]

  ################################################################################
  #                                                                              #
  # Special configuration for SMOG data                                          #
  #                                                                              #
  ################################################################################

  if OnlineEnv.DataType == "PROTONHELIUM16":
    from Configurables import PatPV3D, PVSeed3DTool, PVOfflineTool,LSAdaptPV3DFitter
    from GaudiKernel.SystemOfUnits import mm
    
    pvAlg = PatPV3D("PatPV3D")
    pvAlg.addTool(PVOfflineTool,"PVOfflineTool")
    pvAlg.PVOfflineTool.UseBeamSpotRCut = True
    pvAlg.PVOfflineTool.BeamSpotRCut    = 4.0

    pvAlg.PVOfflineTool.addTool(LSAdaptPV3DFitter())
    pvAlg.PVOfflineTool.PVFitterName = "LSAdaptPV3DFitter"
    
    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.MinTracks    = 3
    pvAlg.PVOfflineTool.LSAdaptPV3DFitter.trackMaxChi2 = 12.0
    
    pvseed = PVSeed3DTool()
    pvseed.zMaxSpread           = 10.0 * mm
    pvseed.MinCloseTracks       = 3
    pvseed.TrackPairMaxDistance = 2.0*mm

    pvAlg.PVOfflineTool.addTool(pvseed)


  # set the options

  class __MonAdd:
    def __init__(self,s):
      self.seq = s
    def addMonitors(self):
      # Read the HLT2 decoders and select a few lines
      from DAQSys.Decoders  import DecoderDB
      from PhysConf.Filters import LoKi_Filters
      
      hlt2Decoder = DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"].setup()

      selectedLines = {"Hlt2PIDD02KPiTagTurboCalib"              : True,
                       "Hlt2PIDLambda2PPiLLTurboCalib"           : True,
                       "Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib"   : True,
                       "Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib"   : True,
                       "Hlt2PIDLambda2PPiLLhighPTTurboCalib"     : True,
                       "Hlt2PIDLambda2PPiLLveryhighPTTurboCalib" : True,
                       "Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib"    : True,
                       "Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib"    : True,
                       "Hlt2PIDDs2PiPhiKKUnbiasedTurboCalib"     : True
                       }

      for l in linesToResurrect:
        if not selectedLines.has_key(l):
          selectedLines[l] = True

      filterCode = ""
      for l in selectedLines.keys():
        if len(filterCode):
          filterCode = filterCode + " | HLT_PASS('" + l + "Decision')"
        else:
          filterCode = "HLT_PASS('" + l + "Decision')"

      hltFiltersForDQ = LoKi_Filters(HLT2_Code  = filterCode)      
      #SMOGPHY GaudiSequencer("HltFilterSeq").Members   += [hlt2Decoder, hltFiltersForDQ.sequence("HltFiltersForDQ")]
      GaudiSequencer("HltFilterSeq").Members   += [hlt2Decoder]

      # Append to processing
      GaudiSequencer("PhysicsSeq").Members += [self.seq]

      # Options to have Herschel ADC plots
      from Configurables import ProcessPhase
      ProcessPhase("Moni").DetectorList += ["HC"]
      from Configurables import HCRawBankDecoder, HCDigitMonitor
      GaudiSequencer("MoniHCSeq").Members += [HCRawBankDecoder(), HCDigitMonitor()]

  mon = __MonAdd(DQMoniSeq)
  Gaudi.appendPostConfigAction(mon.addMonitors)

  EventLoopMgr().OutputLevel = MSG_DEBUG #ERROR
  EventLoopMgr().Warnings    = False

  brunel.UseDBSnapshot = True # try it

  Brunel.Configuration.Brunel.configureOutput = dummy

  HistogramPersistencySvc().OutputFile = ""
  HistogramPersistencySvc().OutputLevel = MSG_ERROR
  print brunel
  return brunel

#============================================================================================================
def setupOnline():
  """
        Setup the online environment: Buffer managers, event serialisation, etc.

        @author M.Frank
  """
  import OnlineEnv

  buffs = ['Events']
  sys.stdout.flush()
  app=Gaudi.ApplicationMgr()
  app.AppName = ''
  app.HistogramPersistency = 'ROOT'
  app.SvcOptMapping.append('LHCb::OnlineEvtSelector/EventSelector')
  app.SvcOptMapping.append('LHCb::FmcMessageSvc/MessageSvc')
  mep = OnlineEnv.mepManager(OnlineEnv.PartitionID,OnlineEnv.PartitionName,buffs,True)
  mep.ConnectWhen = "start";
  sel = OnlineEnv.mbmSelector(input=buffs[0],type='ONE',decode=False,event_type=2)

  #sel.REQ1 = "EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0x2,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"
  #sel.REQ1 = "EvType=2;TriggerMask=0x0,0x0,0x08000000,0x0;VetoMask=0,0x2,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"
  sel.REQ1 = "EvType=2;TriggerMask=0x0,0x0,0x10000000,0x0;VetoMask=0,0x2,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"
  #sel.REQ1 = "EvType=2;TriggerMask=0x0,0x4,0x0,0x0;VetoMask=0x0,0x2,0x0,0x0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"
  #sel.REQ1 = "EvType=2;TriggerMask=0x120,0x0,0x0,0x0;VetoMask=0x0,0x2,0x2,0x0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"
  #sel.REQ1 = "EvType=2;TriggerMask=0x20,0x0,0x0,0x0;VetoMask=0x0,0x2,0x2,0x4;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"
  #sel.REQ1 = "EvType=2;TriggerMask=0x20,0x0,0x0,0x0;VetoMask=0x0,0x2,0x2,0x0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"

  app.EvtSel  = sel
  app.Runable = OnlineEnv.evtRunable(mep)
  app.Runable.NumErrorToStop   = -1
  app.Runable.IgnoreReturnCode =  1
  app.Runable.OnErrorIncident  =  0
  app.ExtSvc.append(mep)
  app.ExtSvc.append(sel)
  app.AuditAlgorithms = False
  app.TopAlg.insert(0,"UpdateAndReset")

  Configs.MonitorSvc().OutputLevel = MSG_ERROR
  Configs.MonitorSvc().UniqueServiceNames = 1
  Configs.MonitorSvc().DimUpdateInterval = 180
  Configs.RootHistCnv__PersSvc("RootHistSvc").OutputLevel = MSG_ERROR
  app.OutputLevel = OnlineEnv.OutputLevel

  # Sleep after stop to allow assembling the histograms by the adders.
  sleeper = LHCb__TransitionSleepSvc("SleepAfter")
  sleeper.StopSleep = 25
  sleeper.FinializeSleep = 25
  app.ExtSvc.append(sleeper)

  configureForking(app)
  sys.stdout.flush()

#============================================================================================================
def patchMessages():
  """
        Messages in the online get redirected.
        Setup here the FMC message service

        @author M.Frank
  """
  import OnlineEnv
  app=Gaudi.ApplicationMgr()
  Configs.AuditorSvc().Auditors = []
  app.MessageSvcType = 'LHCb::FmcMessageSvc'
  if Gaudi.allConfigurables.has_key('MessageSvc'):
    del Gaudi.allConfigurables['MessageSvc']
  msg = Configs.LHCb__FmcMessageSvc('MessageSvc')
  msg.fifoPath      = os.environ['LOGFIFO']
  msg.LoggerOnly    = True
  msg.doPrintAlways = False
  msg.OutputLevel   = OnlineEnv.OutputLevel

#============================================================================================================
def start():
  """
        Finish configuration and configure Gaudi application manager

        @author M.Frank
  """
  import OnlineEnv
  OnlineEnv.end_config(False)
  #OnlineEnv.end_config(True)

def run():
  import traceback
  try:
    br = patchBrunel()
    setupOnline()
    patchMessages()
    start()
  except Exception,X:
    traceback.print_exc()
    info = traceback.format_exc()
    print '[ERROR] Exception:',str(X)
    print '[ERROR] Exception:',str(info)
    sys.exit(1)
