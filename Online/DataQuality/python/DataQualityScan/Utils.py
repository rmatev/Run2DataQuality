import os, sys, time, traceback
import Config

configuration = Config.Config()
__run_fmt = '| %-5s | %6s | %-8s | %-15s | %-15s | %-14s | %-11s | %-11s |'
run_fmt   = __run_fmt
run_hdr   = __run_fmt%('Part','Run','State','Activity','Run Type','Start Time','Todo','Done',)
run_line  = (__run_fmt%('','','','','','','','',)).replace(' ','-').replace('|','+')


# ------------------------------------------------------------------------------
class Utils:
  """
  Data file scanner to steer the data quality processing after HLT2.
  - This class scan the run database for recent runs of the LHCb partition
    --> self.scanDatabase(<partition>)

  \author  M.Frank
  \version 1.0
  """

  # ----------------------------------------------------------------------------
  def __init__(self):
    """ Initializing constructor of the file scanner

    \author  M.Frank
    \version 1.0
    """
    self.config = configuration
    self.__mondb = None
    self.__rundb = None
    self.__archivedb = None
    self.output  = sys.stdout
    
  # ----------------------------------------------------------------------------
  def rund(self):
    import DbAccess
    if not self.__rundb:
      self.__rundb  = DbAccess.RunDB(config=self.config,
                                     db=self.config.rundb)
    return self.__rundb

  # ----------------------------------------------------------------------------
  def mondb(self):
    import DbAccess
    if not self.__mondb:
      self.__mondb  = DbAccess.MonDB(config=self.config,
                                     db=self.config.mondb,
                                     table=self.config.mondb_table)
    return self.__mondb

  # ----------------------------------------------------------------------------
  def archivedb(self):
    import DbAccess
    if not self.__archivedb:
      self.__archivedb  = DbAccess.MonDB(config=self.config,
                                         db=self.config.mondb,
                                         table=self.config.mondb_archive_table)
    return self.__archivedb

  # ----------------------------------------------------------------------------
  def printRun(self, run, output=None):
    if run:
      if not output:
        output = self.output
      tm = time.localtime(run[11])
      start = time.strftime('%Y/%m/%d %H:%M',tm)
      tm = time.localtime(run[2])
      todo = time.strftime('%m/%d %H:%M',tm)
      done = ' n/a '
      if run[3]>0:
        tm = time.localtime(run[3])
        done = time.strftime('%m/%d %H:%M',tm)
      print >> output, run_fmt%(str(run[7]),str(run[0]),str(run[1]),str(run[8]),str(run[9]),\
                                  start[2:],todo,done,)

  # -------------------------------------------------------------------------------
  def printRuns(self, runs, output=None):
    l = len(run_hdr)
    s = '+'
    s = s + (l-2)*'-'
    s = s + '+'
    if output is None:
      print >> self.output, run_line
      print >> self.output, run_hdr
      print >> self.output, run_line
      for run in runs:
        self.printRun(run)
      print >> self.output, run_line
    else:
      out = open(output,'w')
      print >> out, '<HTML><BODY><PRE>'
      print >> out, run_line
      print >> out, '| Dataquality Database dump from  %-75s|'%(time.ctime(),)
      print >> out, run_line
      print >> out, run_hdr
      print >> out, run_line
      for run in runs:
        self.printRun(run,output=out)
      print >> out, run_line
      print >> out, '</PRE></BODY></HTML>'
      out.close()

# -------------------------------------------------------------------------------
utilities = Utils()
# -------------------------------------------------------------------------------
def runsByState(state, archive=False, ordering='ASC', continuous=0, output=None):
  """
  Dump the runs with a given state from the database in a reasonable
  format useful for clients
  """
  print '*** Print runs in state: '+str(state)
  params = {}
  if archive:
    table = configuration.mondb_archive_table
  else:
    table = configuration.mondb_table
  stmt = """SELECT 
                  runid, state, todo, done, checked, archived, 
                  partitionid, partitionname, activity, 
                  runtype, params, starttime
            FROM %s"""%(table,)
  if state:
    if isinstance(state,list) or isinstance(state,tuple):
      stmt = stmt + " WHERE state=:state1"
      params = {'state1': state[0] }
      count = 1
      for s in state[1:]:
        count = count + 1
        stmt = stmt + " OR state=:state%d"%(count,)
        params['state%d'%count] = s
    else:
      stmt = stmt + " WHERE state=:state"
      params = {'state': state }
  stmt = stmt + " ORDER BY runid "+ordering
  db = utilities.mondb()
  while True:
    try:
      cursor = db.execute(stmt, params)
      runs = cursor.fetchall()
      cursor.close()
      utilities.printRuns(runs, output=output)
    except Exception, X:
      print 'Exception while processing statement:', str(X)
    if continuous<=0: break
    time.sleep(float(continuous))

# -------------------------------------------------------------------------------
def runsByNumber(run_numbers,archive=False,ordering='ASC', continuous=0, output=None):
  """
  Dump the runs with a given state from the database in a reasonable
  format useful for clients
  """
  if not len(run_numbers):
    print '*** Cannot print: no run nubers supplied.'
    return False
  
  print '*** Print runs in with run numbers: '+str(run_numbers)
  runs = ''
  for i in run_numbers:
    runs = runs + ' runid='+str(i)+' OR '
  runs = runs[:-4]
  if archive:
    table = configuration.mondb_archive_table
  else:
    table = configuration.mondb_table

  stmt = """SELECT 
                  runid, state, todo, done, checked, archived, 
                  partitionid, partitionname, activity, 
                  runtype, params, starttime
            FROM %s
            WHERE %s
            ORDER BY runid %s"""%(table,runs,ordering,)

  db = utilities.mondb()
  while True:
    try:
      cursor = db.execute(stmt, {})
      runs = cursor.fetchall()
      cursor.close()
      utilities.printRuns(runs, output=output)
    except Exception, X:
      print 'Exception while processing statement:', str(X)
    if continuous<=0: break
    time.sleep(float(continuous))

# -------------------------------------------------------------------------------
def rescheduleRun(run):
  run_no = int(run)
  if utilities.mondb().hasRun(run_no):
    return utilities.mondb().setRunTodo(run_no)
  print '*** Cannot reschedule: Run %d is not known to the database'%(run_no,)
  return None

# -------------------------------------------------------------------------------
def archiveRun(run):
  run_no = int(run)
  if utilities.mondb().hasRun(run_no):
    #return utilities.mondb().setRunTodo(run_no)
    stmt = """SELECT * FROM %s WHERE runid=%s"""%(configuration.mondb_table,run_no,)
    cursor = utilities.mondb().execute(stmt, {})
    runs = cursor.fetchall()
    cursor.close()
    print str(runs)  #utilities.printRuns(runs)
    if len(runs):
      try:
        stmt = """INSERT INTO %s VALUES %s"""%(configuration.mondb_archive_table,str(runs[0]),)
        cursor = utilities.mondb().execute(stmt, {})
        cursor.close()
        stmt = """DELETE FROM %s WHERE runid=%s"""%(configuration.mondb_table,run_no,)
        cursor = utilities.mondb().execute(stmt, {})
        cursor.close()
        utilities.mondb().commit()
        print '*** Successfully moved run %d to the archive table....'%(run_no,)
        return True
      except Exception,X:
        print '*** Cannot archive run %d: %s'%(run_no,str(X),)
      return None
    print '*** Cannot archive: Run %d is not known to the database'%(run_no,)
    return None
  print '*** Cannot archive: Run %d is not known to the database'%(run_no,)
  return None

# -------------------------------------------------------------------------------
def run(run_auto=False):
  runsByState(configuration.st_todo)
  runsByState(configuration.st_running)
  runsByState(configuration.st_done)
  rescheduleRun(123)
  archiveRun(123)

# -------------------------------------------------------------------------------
def showDisplay():
  import Display
  Display.run()
  sys.exit(0)

# -------------------------------------------------------------------------------
def processArgs():
  import optparse
  parser = optparse.OptionParser()
  parser.description = "DataQuality Utilities  [by M.Frank]"

  parser.add_option("-r", "--reschedule", dest="reschedule", default=False,
                    help="Define rescheduling action to be executed",
                    action="store_true", metavar="<boolean>")

  parser.add_option("-a","--archive",
                    dest="archive", default=False,
                    help="Define archiveing action to be scheduled",
                    metavar="<boolean>")

  parser.add_option("-d","--dump",
                    dest="dump", default=False,
                    help="Dump the database content",
                    action="store_true", metavar="<boolean>")

  parser.add_option("-D","--display",
                    dest="display", default=False,
                    help="Show display",
                    action="store_true", metavar="<boolean>")

  parser.add_option("-x","--dump-archive",
                    dest="dump_archive", default=False,
                    help="Dump the database content of the archive_table",
                    action="store_true", metavar="<boolean>")

  parser.add_option("-s", "--state", dest="state", default=None,
                  help="Define state of runs to be dumped",
		  metavar="<string>")

  parser.add_option("-o", "--ordering", dest="ordering", default='ASC',
                  help="Dump runs in ascending order (Default)",
		  metavar="<string>")

  parser.add_option("-f", "--output", dest="output", default=None,
                  help="Output file name for dump [optional]",
		  metavar="<string>")

  parser.add_option("-c", "--continuous", dest="continuous", default=0.0,
                  help="Invoke continuous mode."+
                    "Argument gives sleep time between invocations in seconds.",
		  metavar="<float>")

  (opts, args) = parser.parse_args()
  print 'Options: ',opts, 'Arguments: ',args

  if opts.display:
    showDisplay()
    sys.exit(0)

  if not len(args) and not opts.dump:
    print '     ',len(args),opts.dump,opts.state,\
          not (opts.dump and opts.state),\
          not (opts.dump and (opts.state is not None))
    print '     ',parser.format_help()
  elif opts.reschedule:
    for run in args:   rescheduleRun(run)
    if opts.dump:      runsByNumber(args,archive=opts.dump_archive,ordering=opts.ordering)
  elif opts.archive:
    for run in args:  archiveRun(run)
    if opts.dump:     runsByNumber(args,archive=opts.dump_archive,ordering=opts.ordering)
  elif opts.dump and opts.state:
    exc = None
    state = opts.state
    if state.find(',')>0 and state[0] != '[':
      state = '['+state
    if state.find(',')>0 and state[-1] != ']':
      state = state+']'
    try:
      state = state.replace('[','["').replace(']','"]').replace(',','","')
      runsByState(eval(state),archive=opts.dump_archive,ordering=opts.ordering,\
                    continuous=opts.continuous,output=opts.output)
      exc = None
    except Exception,X:
      exc = X
      try:
        runsByState(eval(str(opts.state)),archive=opts.dump_archive,ordering=opts.ordering,\
                      continuous=opts.continuous,output=opts.output)
        exc = None
      except Exception,Y:
        exc = Y
        try:
          runsByState(opts.state,archive=opts.dump_archive,ordering=opts.ordering,\
                        continuous=opts.continuous,output=opts.output)
          exc = None
        except Exception,Z:
          exc = Z
    if exc: print 'Exc:',str(exc)
  elif opts.dump and len(args):
    runsByNumber(args,archive=opts.dump_archive,ordering=opts.ordering,\
                   continuous=opts.continuous,output=opts.output)
  elif opts.dump:
    cfg = Config.Config()
    runsByState([cfg.st_todo,cfg.st_running,cfg.st_done,cfg.st_failed],archive=opts.dump_archive,\
                  ordering=opts.ordering,continuous=opts.continuous,output=opts.output)
  else:
    print '      ',parser.format_help()
  sys.exit(0)
    
# -------------------------------------------------------------------------------
if __name__=="__main__":
  processArgs()
