import math
import ROOT

from ROOT import gROOT
from ROOT import TF1
from ROOT import TFile
from ROOT import SetOwnership


#----------------------------------------------------------------------------------------
def cb(x, par):
    norm   = par[0]
    mean   = par[1]
    sigma  = abs(par[2])
    alpha  = abs(par[3])
    n      = par[4]

    useLog = False
    if n - 1.0 < 1E-5:
        useLog = True

    sqrtPiOver2 = 1.2533141373
    sqrt2       = 1.4142135624

    a = math.pow(n/alpha, n) * math.exp(-0.5 * alpha * alpha)
    b = n / alpha - alpha

    z = b + alpha

    if -alpha/sqrt2 < -5.0:
        erf = -1.0
    else:
        erf = math.erf(-alpha/sqrt2)

    cbIntL = sigma * a / (n - 1.0) / math.pow(z, n - 1.0) 
    cbIntR = sigma * sqrtPiOver2 * (1 - erf)
    cbInt  = cbIntR + cbIntL

    t = (x[0] - mean) / sigma

    if t < -alpha:
        cbVal =  a/math.pow(b - t, n)
    else:
        cbVal = math.exp(-0.5 * t * t)

    cbVal = norm * cbVal / cbInt

    return cbVal
#----------------------------------------------------------------------------------------
def dcb(x, par):
    norm   = par[0]
    mean   = par[1]
    sigma  = abs(par[2])
    alphaL = abs(par[3])
    nL     = par[4]
    alphaR = abs(par[5])
    nR     = par[6]

    sqrtPiOver2 = 1.2533141373
    sqrt2       = 1.4142135624

    aL = math.pow(nL/alphaL, nL) * math.exp(-0.5 * alphaL * alphaL)
    bL = nL / alphaL - alphaL
    aR = math.pow(nR/alphaR, nR) * math.exp(-0.5 * alphaR * alphaR)
    bR = nR / alphaR - alphaR

    zL = bL + alphaL
    zR = bR + alphaL

    if   alphaR/sqrt2 > 5.0:
        erfR = 1.0
    elif alphaR/sqrt2 < -5.0:
        erfR = -1.0
    else:
        erfR = math.erf(alphaR/sqrt2)

    if   -alphaL/sqrt2 < -5.0:
        erfL = -1.0
    elif -alphaL/sqrt2 > 5.0:
        erfL = 1.0
    else:
        erfL = math.erf(-alphaL/sqrt2)

    dcbIntL = sigma * aL / (nL - 1.0) / math.pow(zL, nL - 1.0)
    dcbIntR = sigma * aR / (nR - 1.0) / math.pow(zR, nR - 1.0) 
    dcbIntC = sigma * sqrtPiOver2 * (erfR - erfL)

    dcbInt = dcbIntL + dcbIntC + dcbIntR

    t = (x[0] - mean) / sigma

    if t < -alphaL:
        dcbVal =  aL/math.pow(bL - t, nL)
    elif t > alphaR:
        dcbVal =  aR/math.pow(bR + t, nR)
    else:
        dcbVal = math.exp(-0.5 * t * t)

    dcbVal = norm * dcbVal / dcbInt
    return dcbVal

#----------------------------------------------------------------------------------------
def fexp(x, par):
    return par[0] * math.exp(par[1] * x[0])
#----------------------------------------------------------------------------------------
def twoSigma(x, par):
    mean       = par[0]
    sigma      = par[1]
    widthRatio = par[3]
    frac       = par[4]

    gauss_1 = math.exp(-math.pow((x[0] - mean)/sigma, 2.0))
    gauss_2 = math.exp(-math.pow((x[0] - mean)/sigma/widthRatio, 2.0))

    return (1 - frac) * gauss_1 + frac * gauss_2
#----------------------------------------------------------------------------------------
def d0MassBackground(x,par):
    return par[0]
#----------------------------------------------------------------------------------------
def d0MassFitFunction(x,par):
    return d0MassBackground(x,par) + cb(x,list(par)[1:])
#----------------------------------------------------------------------------------------
def dstarMassBackground(x,par):
    return par[0] * (math.pow(x[0] - par[1], par[2]) * math.exp(par[3] * x[0]))
#----------------------------------------------------------------------------------------
def dstarMassSignal(x,par):
    return par[0] * twSigma(x, list(par)[4:])
#----------------------------------------------------------------------------------------
def dstarMassFitFunction(x,par):
    return dstarMassBackground(x, par) +  dstarMassSigbal(x, par)
#----------------------------------------------------------------------------------------
def jpsiMassFitFunction(x,par):
    return fexp(x,par) + dcb(x,list(par)[2:])
#----------------------------------------------------------------------------------------
def pi0MassBackground(x,par):
    return par[0] * ( math.log ( par[1]*x[0] ) + par[2]*x[0] )
#----------------------------------------------------------------------------------------
def pi0MassSignal(x,par):
    return par[0] * math.exp(-0.5 * math.pow(((x[0]-par[1])/par[2]),2) )
#----------------------------------------------------------------------------------------
def pi0MassFitFunction(x,par):
    return pi0MassBackground(x,par) + pi0MassSignal(x,list(par)[3:])

#----------------------------------------------------------------------------------------
class DQ_Fits:
    def __init__(self):
        self.file     = None
        self.filename = None
        
        self.resetValues()

        return
#----------------------------------------------------------------------------------------
    def calcMeanNumberOTTimes(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        plot = self.file.Get('OT/OTTimeMonitor/1')
        if not plot:
            print 'DQ_Fits: could not find Track/TrackVertexMonitor/NumPrimaryVertices'
            return False

        SetOwnership(plot, True)

        self.mean_number_of_OT_times    = plot.GetMean()
        self.mean_number_of_OT_timesErr = plot.GetMeanError()

        plot.Delete()
        del plot

        return True
#----------------------------------------------------------------------------------------
    def calcMeanNumberPV(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        plot = self.file.Get('Track/TrackVertexMonitor/NumPrimaryVertices')
        if not plot:
            print 'DQ_Fits: could not find Track/TrackVertexMonitor/NumPrimaryVertices'
            return False

        SetOwnership(plot, True)

        self.mean_number_of_pv    = plot.GetMean()
        self.mean_number_of_pvErr = plot.GetMeanError()

        plot.Delete()
        del plot

        return True
#----------------------------------------------------------------------------------------
    def fileClose(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        if self.file is None:
            return False

        self.file.Close()
        self.file.Delete()

        del self.file

        self.file = None

        return True
#----------------------------------------------------------------------------------------
    def fileOpen(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        if self.filename is None:
            return False
        self.file = TFile.Open(self.filename)

        if self.file.IsZombie():
            self.file = None
            return False

        return True
#----------------------------------------------------------------------------------------
    def fitAll(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        retval = self.fitPi0Mass()
        res    = self.fitD0Mass()
#MAd for now        res    = self.fitDstarMass()
        res    = self.fitJpsiMass()
        retval = retval or res

        return retval
#----------------------------------------------------------------------------------------
    def fitD0Mass(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        massPlot = self.file.Get('MakeDstSel.DaughtersPlots/Mass(D0)')

        if not massPlot:
            return False

        if massPlot.Integral() < 100:
            massPlot.Delete()
            del massPlot
            return False

        SetOwnership(massPlot, True)

        binWidth = massPlot.GetBinWidth(1)
        nEntries = massPlot.Integral()
        nD0 = 0.95 * nEntries

        fitFunc = TF1("d0FitFunc", d0MassFitFunction, 1755, 1955, 6)
        fitFunc.SetParameters(nEntries - nD0, nD0, 1865, 4.0, 2.07, 1.1)
        fitFunc.SetParLimits(0, 0, nEntries)
        fitFunc.SetParLimits(2, 1835, 1895)
        fitFunc.SetParLimits(3, 2, 30)
        fitFunc.SetParLimits(4, 1, 3)
        fitFunc.SetParLimits(5, 1.1, 30)


        fitres = massPlot.Fit("d0FitFunc","QNS","", 1755, 1955)
        if fitres.Status() == 0:
            self.d0YieldVal      = fitFunc.GetParameter(1)/massPlot.GetBinWidth(1)
            self.d0MassVal       = fitFunc.GetParameter(2)
            self.d0ResolutionVal = fitFunc.GetParameter(3)
            
            self.d0YieldErr      = fitFunc.GetParError(1)/massPlot.GetBinWidth(1)
            self.d0MassErr       = fitFunc.GetParError(2)
            self.d0ResolutionErr = fitFunc.GetParError(3)

        massPlot.Delete()
        del massPlot

        return True
#----------------------------------------------------------------------------------------
    def fitDstarMass(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        massPlot = self.file.Get('MakeDstSel.MotherPlots/#Delta Mass')

        if not massPlot:
            return False

        if massPlot.Integral() < 100:
            massPlot.Delete()
            del massPlot
            return False

        SetOwnership(massPlot, True)

        binWidth = massPlot.GetBinWidth(1)
        nEntries = massPlot.Integral()
        nDstar = 0.9 * nEntries


        fitFunc = TF1("dstarFitFunc", dstarMassFitFunction, 140, 154, 9)
        fitFunc.SetParameters(nEntries - nDstar, 130, 2.2, -0.26, nDstar, 145, 0.9, 1.9, 0.1)
        fitFunc.SetParLimits(0, 0, nEntries)
        fitFunc.SetParLimits(1, -200, 140)
        fitFunc.SetParLimits(2, -100, 100)
        fitFunc.SetParLimits(3, -10,   10)
        fitFunc.SetParLimits(4, 0, nEntries)
        fitFunc.SetParLimits(5, 135, 155)
        fitFunc.SetParLimits(6, 0.5, 2.0)
        fitFunc.SetParLimits(7, 1.01, 5)
        fitFunc.SetParLimits(8, 0.0005, 1.0)


        fitres = massPlot.Fit("dstarFitFunc","QNS","", 140, 154)
        if fitres.Status() == 0:
            self.dstarYieldVal      = fitFunc.GetParameter(4)/massPlot.GetBinWidth(1)
            self.dstarDeltaMassVal       = fitFunc.GetParameter(5)
            self.dstarDeltaMassWidthVal = fitFunc.GetParameter(6)
            
            self.dstarYieldErr      = fitFunc.GetParError(4)/massPlot.GetBinWidth(1)
            self.dstarDeltaMassErr       = fitFunc.GetParError(5)
            self.dstarDeltaMassWidthErr = fitFunc.GetParError(6)

        massPlot.Delete()
        del massPlot

        return True
#----------------------------------------------------------------------------------------
    def fitJpsiMass(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        massPlot = self.file.Get('JpsiDQMonitorPlots.MassPlotTool/peak/M_J_psi_1S')

        if not massPlot:
            return False

        if massPlot.Integral() < 100:
            massPlot.Delete()
            del massPlot
            return False

        SetOwnership(massPlot, True)

        binWidth = massPlot.GetBinWidth(1)
        nEntries = massPlot.Integral()
        nJpsi = 0.8 * nEntries

        fitFunc = TF1("jpsiFitFunc", jpsiMassFitFunction, 3000, 3200,9)
        fitFunc.SetParameters(nEntries - nJpsi, -0.001, nJpsi, 3098, 10, 1.7, 2.7, 1.85, 3.0)
        fitFunc.SetParLimits(1, -0.1, 0.1)
        fitFunc.SetParLimits(3, 3092, 3104)
        fitFunc.SetParLimits(4, 8, 15)
        fitFunc.FixParameter(5, 1.7)
        fitFunc.FixParameter(6, 2.7)
        fitFunc.FixParameter(7, 1.85)
        fitFunc.FixParameter(8, 3.0)

        fitres = massPlot.Fit("jpsiFitFunc","QNS","", 3000, 3200)
        
        if fitres.Status() == 0:
            self.jpsiYieldVal      = fitFunc.GetParameter(2)/massPlot.GetBinWidth(1)
            self.jpsiMassVal       = fitFunc.GetParameter(3)
            self.jpsiResolutionVal = fitFunc.GetParameter(4)
            
            self.jpsiYieldErr      = fitFunc.GetParError(2)/massPlot.GetBinWidth(1)
            self.jpsiMassErr       = fitFunc.GetParError(3)
            self.jpsiResolutionErr = fitFunc.GetParError(4)

        massPlot.Delete()
        del massPlot

        return True
#----------------------------------------------------------------------------------------
    def fitPi0Mass(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        massPlot = self.file.Get("CaloMoniDst/ResolvedPi0Mon/4")
        
        if not massPlot:
            return False
        if massPlot.Integral() < 100:
            massPlot.Delete()
            del massPlot
            return False

        SetOwnership(massPlot, True)

        fitFunc = TF1("pi0FitFunc", pi0MassFitFunction,90,200,6)
        fitFunc.SetParameters(100, 3.12587e-02,-4.09403e-03, 100, 135, 10)
        fitFunc.SetParLimits(1,0,1)
        fitFunc.SetParLimits(2,-1,0)
        fitFunc.SetParLimits(4,125,145)
        fitFunc.SetParLimits(5,5,20)
        
        fitres = massPlot.Fit("pi0FitFunc","QNS","", 90,200)
        if fitres.Status() == 0:
            self.pi0massVal       = fitFunc.GetParameter(4)
            self.pi0resolutionVal = fitFunc.GetParameter(5)
            
            self.pi0massErr       = fitFunc.GetParError(4)
            self.pi0resolutionErr = fitFunc.GetParError(5)

        massPlot.Delete()
        del massPlot

        return True
#----------------------------------------------------------------------------------------
    def getFilename(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        return self.filename
#----------------------------------------------------------------------------------------
    def loadAll(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        #
        # Open file and reset everything
        #
        retval = self.fileOpen()
        if not retval:
            self.fileClose()
            return retval

        self.resetValues()

        #
        # Do the fits
        #
        retval = self.fitAll()

        #
        # Calculate variables directly from histograms.
        #

        self.calcMeanNumberPV()
        self.calcMeanNumberOTTimes()

        #
        # Close file and return
        #

        self.fileClose()
        return retval
#----------------------------------------------------------------------------------------
    def resetValues(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        self.mean_number_of_OT_times    = -1.0
        self.mean_number_of_pv          = -1.0
        self.mean_number_of_OT_timesErr = -1.0
        self.mean_number_of_pvErr       = -1.0

        self.d0MassVal              = -1.0
        self.d0ResolutionVal        = -1.0
        self.d0YieldVal             = -1.0
        self.dstarDeltaMassVal      = -1.0
        self.dstarDeltaMassWidthVal = -1.0
        self.dstarYieldVal          = -1.0
        self.jpsiMassVal            = -1.0
        self.jpsiResolutionVal      = -1.0
        self.jpsiYieldVal           = -1.0
        self.pi0massVal             = -1.0
        self.pi0resolutionVal       = -1.0

        self.dstarDeltaMassErr      = -1.0
        self.dstarDeltaMassWidthErr = -1.0
        self.dstarYieldErr          = -1.0
        self.d0MassErr              = -1.0
        self.d0ResolutionErr        = -1.0
        self.d0YieldErr             = -1.0
        self.jpsiMassErr            = -1.0
        self.jpsiResolutionErr      = -1.0
        self.jpsiYieldErr           = -1.0
        self.pi0massErr             = -1.0
        self.pi0resolutionErr       = -1.0

        return
#----------------------------------------------------------------------------------------
    def setFilename(self, filename):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        self.filename = filename
        return
    
