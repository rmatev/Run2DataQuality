import os, sys, time

utgid = os.environ['UTGID']

def copy(fr, to):
  cmd = 'echo "No command assigned yet!'
  try:
    cmd = 'export UTGID='+utgid+'_copy;scp ' + fr + ' ' + to
    ##print 'Executing command:',cmd
    os.system(cmd)
    return True
  except Exception,X:
    print 'Failed to copy file:',cmd,'Exception:',str(X)
  return False

while True:
  comet = 'cron01:/web/sites/comet/production/htdocs/WEB/Online/Stomp/web/DQ'
  try:
    copy('/group/online/dataflow/cmtuser/DQ/DATAQUALITY_OPT/ReaderInput.opts',comet+'/DQ_Input.txt')
    copy('/group/online/dataflow/cmtuser/DQ/DATAQUALITY_OPT/DQ_Dump.html',comet+'/DQ_Dump.html')
    copy(os.environ['DATAQUALITYROOT']+'/python/DQ_Result.html',comet+'/DQ_Result.html')
    out = open('/group/online/dataflow/cmtuser/DQ/DATAQUALITY_OPT/DQ_Files.html','w')
    files = os.listdir('/localdisk/DQ/DATAQUALITY_MON')
    print >>out, '<HTML><BODY><PRE>'
    print >>out, '+---------------------------------------------------------+'
    print >>out, '|  Total of %3d files still to be processed for this run  |'%(len(files),)
    print >>out, '+---------------------------------------------------------+'
    if len(files) > 0:
      for f in files: print >>out,'| /localdisk/DQ/DATAQUALITY_MON/%-25s |'%(f,)
      print >>out, '+---------------------------------------------------------+'
    print >>out, '</PRE></BODY></HTML>'
    out.close()
    files = None
    copy('/group/online/dataflow/cmtuser/DQ/DATAQUALITY_OPT/DQ_Files.html',comet+'/DQ_Files.html')
  except Exception,X:
    print 'Failed to copy file. Exception:',str(X)
  time.sleep(30.0)
