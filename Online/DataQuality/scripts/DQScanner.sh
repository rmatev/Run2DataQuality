#!/bin/bash
#
cd /group/online/dataflow/cmtuser/DataQuality;
. ./setup.x86_64-centos7-gcc62-opt.vars
##echo $PYTHONPATH;
#export PY_EXE=`which python`;
#export PYTHONHOME=`dirname $PY_EXE`;
#export PYTHONHOME=`dirname $PYTHONHOME`;
export UTGID;
export DIM_DNS_NODE=mona10;
export PYTHONPATH=${DATAQUALITYROOT}/python:$PYTHONPATH;
#
echo "DIM-DNS: $DIM_DNS_NODE  Working directory: `pwd`"
#
if test "${1}" = "scan"; then
    exec -a ${UTGID} `which python` <<EOF
import DataQualityScan.DataQualityScanner as Scan
Scan.run(True)
EOF
elif test "${1}" = "dump"; then
    exec -a ${UTGID} `which python` <<EOF
import DataQualityScan.DataQualityScanner as Scan
Scan.dump(True,'/group/online/dataflow/cmtuser/DQ/DATAQUALITY_OPT/DQ_db.html')
EOF
else
    exec -a ${UTGID} `which python` <<EOF
import DataQualityScan.DataQualityScanner as Scan
Scan.check()
EOF
fi;
