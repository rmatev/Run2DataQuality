#!/bin/bash
#
cd /group/online/dataflow/cmtuser/DataQuality/Online/DataQuality/scripts
. Macros.sh
##echo $PYTHONPATH;
export PY_EXE=`which python`;
export PYTHONHOME=`dirname $PY_EXE`;
export PYTHONHOME=`dirname $PYTHONHOME`;
export UTGID;
export DIM_DNS_NODE=mona10;
export PYTHONPATH=${DATAQUALITYROOT}/python:$PYTHONPATH;
#
echo "DIM-DNS: $DIM_DNS_NODE  Working directory: `pwd`"
#
export DQ_CONFIG=${DATAQUALITYROOT}/python/DataQualityScan/DQ_Config.py;
#
#
exec -a ${UTGID} `which python` -c "import DataQualityScan.Utils as U; U.processArgs()" \
    --dump \
    --ordering=desc \
    --continuous=300 \
    --output=/group/online/dataflow/cmtuser/DQ/DATAQUALITY_OPT/DQ_Dump.html
