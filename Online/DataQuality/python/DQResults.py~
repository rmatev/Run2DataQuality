import sys, os, re, time

sys.path.append('/group/online/rundb/RunDatabase//python')
import rundb

sys.path.append('/group/online/dataflow/cmtuser/DataQuality/Online/DataQuality/python/DQDB/dqdb')
import dqdb
import dqdb_params

from DQ_Fits import DQ_Fits

address = 'oracle://' + dqdb_params.login + ':' \
    + dqdb_params.pwd + '@'   \
    + dqdb_params.tns

db  = dqdb.DQ_DB(address)
df  = DQ_Fits()
rdb = rundb.RunDB()

minYmd = 20180927
minHms = 150000

root   = '/hist/Savesets/2018/DQ/DataQuality'

#----------------------------------------------------------------------------------------
def sortList(list, invert=False):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """
  if not invert:
    list.sort(lambda x, y: cmp(x, y))
  else:
    list.sort(lambda x, y: cmp(y, x))
  return list
#----------------------------------------------------------------------------------------
def addNewRun(r, dataPath, runInfo):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """
  runNumber = int(r)

  run      = db.insertRun(runNumber, runInfo['fillId'])
  dataFile = db.addOnlineDQFileForRun(run, dataPath)
  refFile  = setRefFile(run, runInfo)
  dqFlag   = db.setRunDQFlag(run, 'UNCHECKED')

  db.commit()

  return run
#----------------------------------------------------------------------------------------
def runInfo(run):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """
  info   = {}
  retVal = {'magnetState' : None,
            'fillId'      : None,
            'tck'         : None,
            'lumi'        : None,
            'Error'       : '',
            'OK'          : True}
  info = rdb.__getrunparams__(run)

  if info.has_key('magnetState'): 
    retVal['magnetState'] = info['magnetState']
  else:
    retVal['Error'] = 'Run %d has no magnet information' %(int(run))
    retVal['OK'] = False

  if info.has_key('tck'):
    tck = hex(int(info['tck']))
    tck = tck.upper()
    retVal['tck'] = tck.replace('X', 'x') 
  else:
    retVal['Error'] = 'Run %d has no TCK information' %(int(run))
    retVal['OK'] = False
    
  if info.has_key('fillID'):
    retVal['fillId'] = int(info['fillID'])
  else:
    retVal['Error'] = 'Run %d has no fillID information' %(int(run))
    retVal['OK'] = False

  if info.has_key('endLumi'):
    retVal['lumi'] = info['endLumi']/1000000.0
  else:
    retVal['Error'] = 'Run %d has no endLumi information' %(int(run))
    retVal['OK'] = False

  return retVal
#----------------------------------------------------------------------------------------
def findRuns(path):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """

  eorFiles = {}
  for root, dirs,files in os.walk(path):
    for name in files:
      m = re.search('DataQuality-(\d+)-(\d+)T(\d+)-EOR.root', name)
      if not m:
        continue
      run = str(m.group(1))
      ymd = int(m.group(2))
      hms = int(m.group(3))

      
      if ymd < minYmd:
        continue
      if ymd == minYmd and hms < minHms:
        continue
        
      if eorFiles.has_key(run):
        if ymd > eorFiles[run]['ymd']:
          eorFiles[run] = {'ymd'  : ymd,
                           'hms'  : hms,
                           'path' : os.path.join(root, name)}
        elif ymd == eorFiles[run]['ymd']:
          if hms > eorFiles[run]['hms']:
            eorFiles[run] = {'ymd'  : ymd,
                             'hms'  : hms,
                             'path' : os.path.join(root, name)}                    
      else:
        eorFiles[run] = {'ymd'  : ymd,
                         'hms'  : hms,
                         'path' : os.path.join(root, name)}
  return eorFiles            
#----------------------------------------------------------------------------------------
def findReference(tck, magnetState):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """
  path = None

  refFileBase = '/hist/Reference/DQ'
  refFileDir  = refFileBase + '/' + tck + '/' + magnetState

  goodId   = -1
  goodName = None
  for root, dirs, files in os.walk(refFileDir):
    for name in files:
      m = re.search('default_(\d+).root', name)
      if m:
        if int(m.group(1)) > goodId:
          goodId   = int(m.group(1))
          goodName = name

  if goodName is not None:
    path = refFileDir + '/' + goodName

  return path
#----------------------------------------------------------------------------------------
def fitRuns(runData):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """

  for runNumber in sortList(runData.keys()):
    if not runData[runNumber]['UpdatedData']:
      continue

    filename = db.getOnlineDQFile(int(runNumber))
    if filename is None:
      continue
    
    print 'DQResults: [INFO] Now fitting run %s' %(runNumber)

    lumi = runData[runNumber]['lumi']

    df.setFilename(filename)

    fitres = df.loadAll()

    if df.pi0massVal > 0:
      res = db.setRunPropertyValue(runNumber, 'dq_pi0_mass',           df.pi0massVal)
      res = db.setRunPropertyValue(runNumber, 'dq_pi0_mass_err',       df.pi0massErr)
      res = db.setRunPropertyValue(runNumber, 'dq_pi0_resolution',     df.pi0resolutionVal)
      res = db.setRunPropertyValue(runNumber, 'dq_pi0_resolution_err', df.pi0resolutionErr)

      print 'DQResults: [INFO] pi0 mass        = %f +/- %f' %(df.pi0massVal,       df.pi0massErr)
      print 'DQResults: [INFO] pi0 resolution  = %f +/- %f' %(df.pi0resolutionVal, df.pi0resolutionErr)

    if df.jpsiYieldVal > 0:
      if not lumi == 0:
        res = db.setRunPropertyValue(runNumber, 'dq_jpsi_yield',          df.jpsiYieldVal/lumi)
        res = db.setRunPropertyValue(runNumber, 'dq_jpsi_yield_err',      df.jpsiYieldErr/lumi)

        print 'DQResults: [INFO] J/psi yield      = %f +/- %f' %(df.jpsiYieldVal/lumi, df.jpsiYieldErr/lumi)
  
      res = db.setRunPropertyValue(runNumber, 'dq_jpsi_mass',           df.jpsiMassVal)
      res = db.setRunPropertyValue(runNumber, 'dq_jpsi_mass_err',       df.jpsiMassErr)
      res = db.setRunPropertyValue(runNumber, 'dq_jpsi_resolution',     df.jpsiResolutionVal)
      res = db.setRunPropertyValue(runNumber, 'dq_jpsi_resolution_err', df.jpsiResolutionErr)

      print 'DQResults: [INFO] J/psi mass       = %f +/- %f' %(df.jpsiMassVal,       df.jpsiMassErr)
      print 'DQResults: [INFO] J/psi resolution = %f +/- %f' %(df.jpsiResolutionVal, df.jpsiResolutionErr)

    if df.d0YieldVal > 0:
      if not lumi == 0:
        res = db.setRunPropertyValue(runNumber, 'dq_D0_yield',          df.d0YieldVal/lumi)
        res = db.setRunPropertyValue(runNumber, 'dq_D0_yield_err',      df.d0YieldErr/lumi)

        print 'DQResults: [INFO] D0 yield      = %f +/- %f' %(df.d0YieldVal/lumi, df.d0YieldErr/lumi)
  
      res = db.setRunPropertyValue(runNumber, 'dq_D0_mass',           df.d0MassVal)
      res = db.setRunPropertyValue(runNumber, 'dq_D0_mass_err',       df.d0MassErr)
      res = db.setRunPropertyValue(runNumber, 'dq_D0_resolution',     df.d0ResolutionVal)
      res = db.setRunPropertyValue(runNumber, 'dq_D0_resolution_err', df.d0ResolutionErr)

      print 'DQResults: [INFO] J/psi mass       = %f +/- %f' %(df.jpsiMassVal,       df.jpsiMassErr)
      print 'DQResults: [INFO] J/psi resolution = %f +/- %f' %(df.jpsiResolutionVal, df.jpsiResolutionErr)

    if df.mean_number_of_pv > 0:
      res = db.setRunPropertyValue(runNumber, 'dq_mean_number_of_PV',     df.mean_number_of_pv)
      res = db.setRunPropertyValue(runNumber, 'dq_mean_number_of_PV_err', df.mean_number_of_pvErr)

      print 'DQResults: [INFO] Mean number of PV = %f +/- %f' %(df.mean_number_of_pv, df.mean_number_of_pvErr)
  
    if df.mean_number_of_OT_times > 0:
      res = db.setRunPropertyValue(runNumber, 'dq_mean_number_of_OT_times',     df.mean_number_of_OT_times)
      res = db.setRunPropertyValue(runNumber, 'dq_mean_number_of_OT_times_err', df.mean_number_of_OT_timesErr)

      print 'DQResults: [INFO] Mean number of PV = %f +/- %f' %(df.mean_number_of_OT_times, df.mean_number_of_OT_timesErr)
      
    db.commit()

  return
#----------------------------------------------------------------------------------------
def loadRunData(eorFiles):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """
  retval = {'OK'   : True,
            'runs' : {}
            }

  for runNumber in sortList(eorFiles.keys()):
    retval['runs'][runNumber] = {}

    #
    # Load run information from the rundb
    #

    rInfo = runInfo(runNumber)
    if not rInfo['OK']:
      print 'DQResults: [WARN] %s' %(rInfo['Error'])
      retval['OK'] = False
      return retval

    for k in rInfo.keys():
      retval['runs'][runNumber][k] = rInfo[k]

    #
    # Add the run to the DQ_DB if needed
    #

    run = db.getRun(int(runNumber))

    if run:
      retval['runs'][runNumber]['run']         = run
      retval['runs'][runNumber]['UpdatedData'] = False
      retval['runs'][runNumber]['UpdatedRef']  = False
    else:
      run = addNewRun(runNumber,
                      eorFiles[runNumber]['path'],
                      retval['runs'][runNumber])
      retval['runs'][runNumber]['run']         = run
      retval['runs'][runNumber]['UpdatedData'] = True
      retval['runs'][runNumber]['UpdatedRef']  = False

    #
    # Check if the data file or reference file need updating
    #

    res = updateRunData(run,
                        eorFiles[runNumber]['path'],
                        retval['runs'][runNumber])
    if not res['OK']:
      retval['OK'] = false
      break

    if res['UpdatedData']:
      retval['runs'][runNumber]['UpdatedData'] = True
    if res['UpdatedRef']:
      retval['runs'][runNumber]['UpdatedRef']  = True

  return retval
#----------------------------------------------------------------------------------------
def makeRunHTML():
  """ 
  \author  M. Adinolfi
  \version 1.0
  """

  # UNCHECKED runs
  runs    = db.getRunsWithDQFlag('UNCHECKED', 172932)
  nMaxCol = 10

  f = open('DQ_Result.html', 'w')
  outmess = '<!DOCTYP<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\n<HTML>\n<HEAD>\n</HEAD>\n<BODY>\n'
  f.write(outmess)

  outmess = '<TABLE CELLPADDING="1" CELLSPACING="1" BORDER="1" WIDTH="100%">\n'
  f.write(outmess)

  outmess = '<TR><TD WIDTH="50%" VALIGN="TOP">\n<TABLE CELLPADDING="1" CELLSPACING="1" BORDER="1" WIDTH="100%">\n'
  f.write(outmess)

  outmess = '<TR><TD COLSPAN="' + str(nMaxCol) + '" ALIGN="CENTER" VALIGN="TOP" BGCOLOR="#EEAA00">'+\
      time.strftime('Unchecked Runs  [%T %D]',time.localtime())+'</TD></TR>'
  f.write(outmess)

  for i in range(0, len(runs), nMaxCol):
    outmess = '<TR>'
    for j in range(0, nMaxCol):
      if i+j < len(runs):
        outmess = outmess + '<TD ALIGN="CENTER">' + str(runs[i+j]) + '</TD>'
      else:
        break
    outmess = outmess + '</TR>\n'
    f.write(outmess)

  outmess = '</TABLE>\n</TD>\n'
  f.write(outmess)

  # UNKNOWN runs

  runs    = db.getRunsWithDQFlag('UNKNOWN', 172932)

  outmess = '<TD WIDTH="50%" VALIGN="TOP">\n<TABLE CELLPADDING="1" CELLSPACING="1" BORDER="1" WIDTH="100%">\n'
  f.write(outmess)

  outmess = '<TR><TD COLSPAN="' + str(nMaxCol) + '" ALIGN="CENTER" VALIGN="TOP" BGCOLOR="#EEAA00">'+\
      time.strftime('Unknown Runs  [%T %D]',time.localtime())+'</TD></TR>'
  f.write(outmess)

  for i in range(0, len(runs), nMaxCol):
    outmess = '<TR>'
    for j in range(0, nMaxCol):
      if i+j < len(runs):
        outmess = outmess + '<TD ALIGN="CENTER">' + str(runs[i+j]) + '</TD>'
      else:
        break
    outmess = outmess + '</TR>\n'
    f.write(outmess)

  outmess = '</TABLE>\n</TD>\n'
  f.write(outmess)

  outmess = '</TABLE>\n</TD></TR>\n'
  f.write(outmess)
  outmess = '</TABLE>\n</BODY>\n</HTML>\n'
  f.write(outmess)
  f.close()

  return
#----------------------------------------------------------------------------------------
def setRefFile(run, rInfo):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """
  path = findReference(rInfo['tck'], rInfo['magnetState'])

  if path is None:
    print 'DQResults: [INFO] No reference known for run %d, TCK = %s, magnet state = %s' \
               %(run.runNumber, rInfo['tck'], rInfo['magnetState'])
    return path

  return db.addOnlineDQRefFile(run, path)
#----------------------------------------------------------------------------------------
def updateDataFile(run, path):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """
  db.updateOnlineDQDataFile(run, path)
  print 'DQResults: [INFO] Data file for run %s updated to %s' %(run.runNumber, path)
  return True

#----------------------------------------------------------------------------------------
def updateReference(run, runData):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """

  path = findReference(runData['tck'], runData['magnetState'])

  if path is None:
    ##print 'DQResults: [WARN] Cannot update reference: No reference known for run %d, TCK = %s, magnet state = %s' \
    ##           %(run.runNumber, runData['tck'], runData['magnetState'])
    return None

  db.updateOnlineDQRef(run, path)
  print 'DQResults: [INFO] Reference file for run %s updated to %s' %(run.runNumber, path)
  return True
#----------------------------------------------------------------------------------------
def updateRunData(run, dataFilePath, runData):
  """ 
  \author  M. Adinolfi
  \version 1.0
  """
  retval = {'OK'          : True,
            'UpdatedData' : False,
            'UpdatedFlag' : False,
            'UpdatedRef'  : False}

  #
  # Update the data quality flag if missing
  #
  
  if run.dqFlag is None:
    flag = 'UNCHECKED'
    res  = db.setRunDQFlag(run, flag)
    
    if res['OK'] and res['Modified']:
      print 'DQResults: [INFO] Run %s flag is now set to %s'      %(str(run.runNumber), flag)
      retval['UpdatedFlag'] = True
    elif res['OK']:
      print 'DQResults: [INFO] Run %s flag was already set to %s' %(str(run.runNumber), flag)
    else:
      print 'DQResults: [WARN] Cannot set run %s flag to %s'      %(str(run.runNumber), flag)
      retval['OK'] = False
      return retval

  #
  # Update the reference file if missing
  #

  refPath = db.getOnlineDQRef(run.runNumber)
  if refPath is None:
    res = updateReference(run, runData)
    if res:
      retval['UpdatedRef'] = True
  #
  # Update the data file if changed.
  #

  if len(run.dataFile) == 0:
    dataFile = db.addOnlineDQFileForRun(run, dataFilePath)
    retval['UpdatedData'] = True
  else:
    found = False
    for dfile in run.dataFile:
      if dfile.dataFilePath == dataFilePath:
        found = True
        break

    if not found:
      res = updateDataFile(run, dataFilePath)
      if res:
        retval['UpdatedData'] = True

  doCommit = retval['UpdatedData'] or retval['UpdatedFlag'] or retval['UpdatedRef']

  if doCommit:
    db.commit()

  return retval
###############################################################################
# 
#
#         Main starts here
#
#
###############################################################################

while 1:
  eorFiles = findRuns(root)
  runData  = loadRunData(eorFiles)

  if not runData['OK']:
    break

  res = fitRuns(runData['runs'])

  makeRunHTML()
  time.sleep(300)




