#!/bin/bash
source /group/online/dataflow/cmtuser/DataQuality/setup.x86_64-centos7-gcc62-opt.vars;
export PYTHONPATH=${DATAQUALITYROOT}/python:$PYTHONPATH;
DATAQUALITY_UTILITIES()
{
  CFG=${1};
  shift;
  export DQ_CONFIG=${DATAQUALITYROOT}/python/DataQualityScan/${CFG}_Config.py;
  `which python` -c "import DataQualityScan.Utils as U; U.processArgs()" $*;
}
#alias h2_util='DATAQUALITY_UTILITIES H2';
#alias h2_reschedule='DATAQUALITY_UTILITIES H2 -r -d ';
#alias h2_archive='DATAQUALITY_UTILITIES H2 -a -d';
#alias h2_dump='DATAQUALITY_UTILITIES H2 -d';

alias dq_util='DATAQUALITY_UTILITIES DQ';
alias dq_reschedule='DATAQUALITY_UTILITIES DQ -r -d ';
alias dq_archive='DATAQUALITY_UTILITIES DQ -a -d';
alias dq_dump='DATAQUALITY_UTILITIES DQ -d';
alias dq_display='DATAQUALITY_UTILITIES DQ -D';
#
dq_killall()  {
    tmKill -s 9 DQ_MONA100\*;
    tmKill -s 9 MONA100?_R_Controller;
}
#
dq_files()   {
    echo "+++++ Files pending for DQ analysis:";
    echo "------------------------------------";
    ls -l /localdisk/DQ/DATAQUALITY_MON;
    ls -l /localdisk/DQ/DATAQUALITY_MON | wc;
}
#
cat <<EOF
Data quality utilities:
-----------------------
dq_killall                    Kill all processes participating 
                              in the data quality monitoring.
dq_reschedule <run-number>    Reschedule a run for processing.
dq_archive    <run-number>    Move a run in the database to the 
                              archive section.
dq_dump                       Dump DQ database content.
dq_files                      List the files still pending for processing.
EOF
