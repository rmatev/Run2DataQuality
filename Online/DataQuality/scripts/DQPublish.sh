#!/bin/bash
export UTGID;
cd /group/online/dataflow/cmtuser/DataQuality
. ./setup.x86_64-centos7-gcc62-opt.vars
#
#
export PY_EXE=`which python`;
export PYTHONHOME=`dirname $PY_EXE`;
export PYTHONHOME=`dirname $PYTHONHOME`;
export PYTHONPATH=${DATAQUALITYROOT}/python:$PYTHONPATH;
#
exec -a ${UTGID} `which python` -u ${DATAQUALITYROOT}/python/DQPublish.py;
